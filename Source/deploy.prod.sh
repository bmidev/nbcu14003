#!/bin/bash

# Site = user@example.com
site="nbcu14003@pitchperfect2.brandmovers.net"

# Synchronize files.
rsync -rvihz --exclude-from='./.rsync-filter' --rsh=ssh ./htdocs/ "${site}:~/htdocs/"
rsync -rvihz --exclude-from='./.rsync-filter' --rsh=ssh ./ "${site}:~/"

# Set folder permissions
ssh $site 'chmod 0777 ~/application/data ~/application/cache ~/application/logs ~/htdocs/data'

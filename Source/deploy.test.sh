#!/bin/bash

# Site = user@example.com
site="bmorse@bmorse-tst.brandmovers.net"

# Synchronize files.
rsync -rvithz --exclude-from='./.rsync-filter' --rsh=ssh ./htdocs/ "${site}:~/htdocs/"
rsync -rvithz --exclude-from='./.rsync-filter' --rsh=ssh ./ "${site}:~/"

# Set folder permissions
#ssh $site 'chmod 0777 ~/application/data ~/application/cache ~/application/logs ~/htdocs/data'

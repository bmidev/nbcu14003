Brandmovers CodeIgniter Framework
=================================

Installation
------------

[Composer](http://getcomposer.org/) is the recommended way to install the base framework and any modules needed into a new (blank) project.

To install, run the following commands:

```
$ cd ~/Sites/your-project/Source
$ composer create-project --stability="dev" --repository-url="https://gitlab.brandmovers.net/packages" ci/base ./
$ rm -Rf `find ./ -type d -name .git`
```

To add modules from [Packagist](http://packagist.org) or from [Brandmovers private repository](https://gitlab.brandmovers.net/packages), simply edit the `require` block in your `composer.json` and then run `composer update`.

**IMPORTANT:** After Composer finishes downloading all the dependencies that you specified for the project, you need to remove all the `.git` directories or Git will see them as submodules and cause problems when you try to commit.

```
rm -Rf `find ~/Sites/your-project/Source -type d -name .git`
```

Prelaunch Filtering
-------------------

Features:

* Blocks visitors from accessing the site prior to launch, while allowing testers to access the site prior to launch through an activation link
* Allows Facebook scrapers to access the site prior to launch
* Allows CLI scripts to run prior to launch
* Users who use the activation link will be activated for a single browsing session

Configuration instructions:

1. Randomly generate a token string to replace the one in index.php and prelaunch.php (each promotion should have a new token)
2. Change the start date in index.php to your project's actual launch date

The activation link uses the token string you generated. For instance, using the default token string, the URL would look like this:

```
http://your-domain.com/prelaunch.php?token=nzooylu4ZJFjJfK03ItCSrJ65wVchWkuXHOtFIBD8JWgmxBP3xprsChcyBqPTmX
```

Deployment
----------

To deploy, run the following:

```
cd ~/Sites/your-project/Source/
./deploy.sh user@hostname
```

Replace `user` and `hostname` with the actual username and server hostname you wish to deploy to.

After deploying, don't forget to SSH into the server and run any database migrations that may be required:

```
$ php htdocs/index.php tools migrate --latest
```

Third-Party components included
-------------------------------

* [FirePHP 0.3.2](http://www.firephp.org/)
* [Modular Extensions](https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc)
* [Phil Sturgeon's Template Library](https://github.com/philsturgeon/codeigniter-template)
* [PHP dotenv](https://github.com/vlucas/phpdotenv)

Changelog
---------

* 0.5.2
  - Changed from being a `codeigniter-third-party` package to a project package, and updated the installation instructions
  - Added a cookie-based prelaunch access control mechanism courtesy of Eric Stewart
  - Added a standard P3P header to allow cookies in an IFrame context in Internet Explorer
* 0.5.1
  - Added `MY_Form_validation.php` with Brandmovers proprietary validation rules and english/spanish language files
* 0.5.0
  - Added support for [Composer](http://getcomposer.org)
  - Added support for [PHPDotEnv](https://github.com/vlucas/phpdotenv)
  - Added an entities loader compatibility with Modular Extensions
  - Added `cache.php` and `memcached.php` config files
  - Added an `application/entities` directory
  - Added a `deploy.sh` script
  - Removed the `tools.php` controller (moved to the `ci/tools` module installable via Composer)
  - Applied `realpath()` to the system and application directory paths so that they are absolute
  - Set the log and cache directories to absolute paths
  - Set the PHP `error_log` to `application/logs/php-YYYY-MM-DD.log`
  - Set the `database.php` config file to use getenv()
  - Set the `index_page` config setting to blank
  - Improved documentation (CHANGELOG.md, THIRDPARTY.md, DEPLOY.md, INSTALL.md)
  - Moved all documentation into a `docs` directory
* 0.4.1
  - Updated to CodeIgniter 2.1.2
  - Updated [HMVC Extension](https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc/wiki/Home) to revision f972fec54f21
  - Updated [Phil Sturgeon's Template Library](https://github.com/philsturgeon/codeigniter-template) to revision 78525c19df0023908a1e560291e443b2cc5482cf
* 0.4.0
  - Added migration tool
  - Added file deployment tool
  - Added data directories to application and htdocs for storing user uploaded files
* 0.3.1
  - Removed the Sparks tool kit
* 0.3.0
  - Implement Template system into base CI
  - Include Sparks into base install
  - Include FirePHP
* 0.2.0
  - Environment settable using Apache and Shell environment variables
  - Added HMVC extension. This provides support for module separation as well as hierarchical support
* 0.1.0
  - Imported CodeIgniter 2.1.0
  - Disabled CodeIgniter's error handler
  - Set database setting pconnect to FALSE
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_sessions_20120814132600 extends CI_Migration {
	
	/**
	 * Class constructor.
	 */
	public function _construct()
	{
		// Load the database.
		$this->load->database();
	}
	
	/**
	 * Adds the ci_sessions table for supporting CI Session.
	 */
	public function up()
	{
		// Define the table.
		$sql = "CREATE TABLE IF NOT EXISTS  `ci_sessions` (
							session_id varchar(40) DEFAULT '0' NOT NULL,
							ip_address varchar(45) DEFAULT '0' NOT NULL,
							user_agent varchar(120) NOT NULL,
							last_activity int(10) unsigned DEFAULT 0 NOT NULL,
							user_data text NOT NULL,
							PRIMARY KEY (session_id),
							KEY `last_activity_idx` (`last_activity`)
						) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARACTER SET UTF8 COLLATE UTF8_UNICODE_CI;";
		
		// Run it.
		$this->db->query($sql);
	}
	
	/**
	 * Drop the ci_sessions table.
	 */
	public function down()
	{
		// Define the table.
		$sql = "DROP TABLE `ci_sessions`;";
		
		// Run it.
		$this->db->query($sql);
	}
}


?>
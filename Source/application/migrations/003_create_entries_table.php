<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Create_entries_table extends CI_Migration
{
	public function _construct()
	{
		// Load the database.
		$this->load->database();
	}
	
	public function up()
	{
		$sql = <<<SQL
CREATE TABLE `entries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(120) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `caption` blob,
  `official_rules` int(1) DEFAULT '0',
  `optin` int(1) DEFAULT '0',
  `created_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SQL;
		foreach (explode(';', $sql) as $query)
		{
			if (empty($query)) continue;
			$this->db->query(trim($query));
		}
	}
	
	public function down()
	{
		$sql = 'DROP TABLE IF EXISTS `entries`';
		$this->db->query($sql);
	}
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Dashboard_users extends CI_Migration
{
	public function _construct()
	{
		// Load the database.
		$this->load->database();
	}

	public function up()
	{
		$sql = <<<SQL
INSERT INTO `dashboard_users` (`id`, `status`, `email`, `name`, `password`, `created_at`)
VALUES
	(2, 'Active', 'bmorse@brandmovers.com', 'Brad Morse', '75471707fd277fb79e9cd8ff14d51351a1162c64', '2014-06-16 13:43:03'),
	(3, 'Active', 'client@brandmovers.com', 'client@brandmovers.com', '7fc099825a9b383d02d4f77e239b34bdabdb6158', '2014-06-16 13:44:05');

SQL;
		$this->db->query($sql);
	}
	
	public function down()
	{
		$sql = 'DELETE FROM `dashboard_users` WHERE `id` > 1';
		$this->db->query($sql);
	}
}


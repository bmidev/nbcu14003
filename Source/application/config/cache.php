<?php

/*
|--------------------------------------------------------------------------
| Cache settings
|--------------------------------------------------------------------------
*/

switch (ENVIRONMENT)
{
	case 'development':
	case 'testing':
		$config['cache_adapter'] = array('adapter' => 'file');
		$config['cache_ttl'] = 15 * 60; // 15 minutes
	break;
	
	case 'production':
		$config['cache_adapter'] = array('adapter' => 'memcached');
		$config['cache_ttl'] = 15 * 60; // 15 minutes
	break;
}


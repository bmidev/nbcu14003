<?php

// Application specific functions.
define('NOW', time());
define('NOW_MICRO', microtime(true));

// CDN Hostnames by environment
$config['cdn_hostname']['production'] = 'prod.example.com';
$config['cdn_hostname']['testing'] = 'test.example.com';
$config['cdn_hostname']['development'] = NULL;

// Application Version.
$config['app_version'] = '0.1';


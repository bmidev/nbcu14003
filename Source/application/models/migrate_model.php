<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migrate_model extends CI_Model {
	
	/**
	 * Return the current migration or 0 if not migrations have been found.
	 */
	public function current_migration()
	{
		$sql = "SHOW TABLES LIKE 'migrations'";
		$result = $this->db->query($sql);
		if ($result->num_rows() == 0)
		{
			$result->free_result();
			return 0;
		}
		
		$sql = 'SELECT `version` FROM `migrations`;';
		$result = $this->db->query($sql);
		if ($result->num_rows() == 0)
		{
			$result->free_result();
			return 0;
		}
		
		$array = $result->row_array();
		$result->free_result();
		return (int)$array['version'];
	}

}
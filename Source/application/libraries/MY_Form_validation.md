CodeIgniter Form Validation Library Extension
=============================================

Installation
------------

1. [Require this package](http://getcomposer.org/doc/00-intro.md#declaring-dependencies) in your `composer.json` file:

```json
{
    "require": {
        "ci/form_validation": "*"
    }
}
```

2. Run `composer update` from the command line.

> **Note:** If you are using controller callbacks to do form validation within a
> [modular extension](https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc/wiki/Home),
> you will need to install this library as it exposes the `$CI` property as public.

Rule Reference
--------------

This extension to CodeIgniter's form_validation library adds the following rules:

```
Rule                       | Parameter                               | Description                                       
---------------------------|-----------------------------------------|---------------------------------------------------
`valid_name`               |                                         | Alphanumeric + ` `, `'`, `-`, `.`
---------------------------|-----------------------------------------|---------------------------------------------------
`agree_to_rules`           |                                         | Checks for `Yes` (equivalent to `in[Yes]`)
---------------------------|-----------------------------------------|---------------------------------------------------
`exists`                   | `[table.column]`                        | Checks if the value exists in a database.
                           |                                         | Example: `exists[codes.code]`
---------------------------|-----------------------------------------|---------------------------------------------------
`in`                       | `[item1,item2,...]`                     | Checks against a list of values.
                           |                                         | Example: `in[Yes,No]`
---------------------------|-----------------------------------------|---------------------------------------------------
`valid_date`               |                                         | Checks a date (`mm/dd/yyyy` format)
---------------------------|-----------------------------------------|---------------------------------------------------
`valid_date_multipart`     | `[month_field/day_field/year_field]`    | Checks a date assembled from separate fields.
                           |                                         | Example: `valid_date_multipart[month/day/year]`
---------------------------|-----------------------------------------|---------------------------------------------------
`valid_age`                | `[18]`                                  | Checks a minimum age against a date field.
                           |                                         | Example: `valid_age[18]`
---------------------------|-----------------------------------------|---------------------------------------------------
`valid_age_multipart`      | `[18/month_field/day_field/year_field]` | Checks a minimum age assembled from separate
                           |                                         | fields.
                           |                                         | Example: `valid_age[18/dob_month/dob_day/dob_year]`
---------------------------|-----------------------------------------|---------------------------------------------------
`valid_state_abbr`         |                                         | Valid U.S. State abbreviation
---------------------------|-----------------------------------------|---------------------------------------------------
`valid_country`            |                                         | Valid country abbreviation
---------------------------|-----------------------------------------|---------------------------------------------------
`valid_province_abbr`      |                                         | Valid province abbreviation (Canada)
---------------------------|-----------------------------------------|---------------------------------------------------
`valid_postal_code_format` |                                         | Valid British postal code
---------------------------|-----------------------------------------|---------------------------------------------------
`valid_phone`              |                                         | Exactly 10 numeric digits, after stripping
                           |                                         | non-numeric chars
---------------------------|-----------------------------------------|---------------------------------------------------
`valid_phone_us`           |                                         | Valid U.S. phone number
---------------------------|-----------------------------------------|---------------------------------------------------
`email_not_fraudulent`     | `[action]`                              | Checks the email against an action list for
                           |                                         | fraudulent activity*.
                           |                                         | Example: `email_not_fraudulent[registration]`
---------------------------|-----------------------------------------|---------------------------------------------------
`valid_recaptcha`          |                                         | Checks a reCAPTCHA `challenge\|response` string**
```

> \*Requires the `ci/security` module

> \** Requires the `ci/recaptcha` module

Filter Reference
----------------

```
Name                | Description
--------------------|---------------------------------------------------------------
`strip_non_numeric` | Removes any non-numeric character
--------------------|---------------------------------------------------------------
`humanize`          | Replaces `_` with ` `, capitalizes words, and trims whitespace
```

Method Reference
----------------

This extension adds the following methods for use in your controllers:

### `$this->form_validation->set_errors();`

Use to set custom error messages in the form. Requires an array in `array('field_name' => 'error message')` format.

### `$this->form_validation->get_errors();`

Returns an associative array of errors in `array('field_name' => 'error message')` format.

Function Reference
------------------

This extension adds the following functions for use in your views:

### `form_error_class($field_name, $class = 'error')`

This function will return an HTML class name if there is an error validating the field. This is useful for highlighting fields that have errors.

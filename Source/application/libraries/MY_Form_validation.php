<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation 
{
	public $CI;

	public function trace($value, $params)
	{
		var_dump($value);
		return TRUE;
	}

	public function valid_name($value)
	{
		return ( ! preg_match('/[^a-zA-Z0-9 \-\.\']/', $value));
	}
  	
	public function agree_to_rules($value, $params)
	{
		return (isset($value) && $value == 'Yes');
	}
	
	public function exists($str, $field)
	{
		list($table, $field) = explode('.', $field);
		$query = $this->CI->db->get_where($table, array($field => $str));
		return $query->num_rows() > 0;
    }
    
    public function in($value, $params)
    {
    	$set = explode(',', $params);
    	
    	if (count($set) === 1)
    	{
    		$this->set_message('in', str_replace('{a}', $set[0], lang('in_1')));
	    }
    	elseif (count($set) === 2)
    	{
    		$this->set_message('in', str_replace(array('{a}', '{b}'), $set, lang('in_2')));
	    }
    	else
    	{
    		$this->set_message('in', lang('in_n').implode(', ', $set));
	    }
	    
		return in_array($value, $set);
    }
    
    public function valid_recaptcha($recaptcha)
    {
    	$this->CI->load->helper('recaptcha');
    	
    	list($challenge, $response) = explode('|', $recaptcha);
    	$response = recaptcha_check_answer(
    		config_item('recaptcha_private_key'),
    		$this->CI->input->ip_address(),
    		$challenge,
    		$response
    	);
    	
    	return (boolean) $response->is_valid;
    }
	
	// =========
	// ! Email
	// =========
	
  	public function email_not_fraudulent($email, $action) 
  	{
  		$this->CI->load->library('fraud_detection');
		
		// Check the IP address
		$ip = $this->CI->input->ip_address();
		if ( ! $this->CI->fraud_detection->check($ip, 'ip', $action))
		{
			$this->set_message('email_not_fraudulent', $this->CI->fraud_detection->get_last_error());
			return false;
		}
		
		// Check the email domain
		$domain = email_hostname($email);
		if ( ! $this->CI->fraud_detection->check($domain, 'domain', $action))
		{
			$this->set_message('email_not_fraudulent', $this->CI->fraud_detection->get_last_error());
			return false;
		}
		
		return TRUE;  	
  	}
  	
	// ==================
	// ! Dates and Ages
	// ==================
	
	public function valid_date($value, $params) 
	{
		if (strpos($value, '/') === FALSE) 
		{
			return false;
		}
		else
		{
			$parts = explode('/', $value);
			return @checkdate($parts[0], $parts[1], $parts[2]);
		}
	}
	
	public function valid_date_multipart($value, $params)
	{
		list($month_field, $day_field, $year_field) = explode('/', $params);
		$year = $this->CI->input->post($year_field);
		$date = sprintf(
			strlen($year) === 4 ? '%02d/%02d/%04d' : '%02d/%02d/%02d',
			(int) $this->CI->input->post($month_field),
			(int) $this->CI->input->post($day_field),
			(int) $year
		);
		
		return $this->valid_date($date, NULL);
	}
	
  	public function valid_age($value, $params = 18) 
  	{
		$this->set_message('valid_age', str_replace('{age}', $params, lang('valid_age')));
		
		if ( ! isset($value) || ! $value || ! strtotime($value)) 
		{
			$this->set_message('valid_age', lang('valid_date'));
			return FALSE;
		}
 		elseif (strtotime($value) > strtotime("-$params year")) 
 		{ 
 			$this->CI->load->library('fraud_detection');
			$this->CI->fraud_detection->log("Registration under $params years of age: " . $value);
			//$this->CI->input->set_cookie('bmi_clrx005_age', 'block', -86400);
			//redirect('/');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
  	}
  	
	public function valid_age_multipart($value, $params)
	{
		list($age, $month_field, $day_field, $year_field) = explode('/', $params);
		$this->set_message('valid_age_multipart', str_replace('{age}', $age, lang('valid_age')));
		
		$year = $this->CI->input->post($year_field);
		$date = sprintf(
			strlen($year) === 4 ? '%02d/%02d/%04d' : '%02d/%02d/%02d',
			(int) $this->CI->input->post($month_field),
			(int) $this->CI->input->post($day_field),
			(int) $year
		);
		
		return $this->valid_age($date, $age);
	}
	
	// =============
	// ! Addresses   
	// =============
  	
  	public function valid_state_abbr($value, $params) 
  	{
		$us_states = array('AL','AK','AZ','AR','CA','CO','CT','DE','DC','FL','GA',
						   'HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA',
						   'MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY',
						   'NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX',
						   'UT','VT','VA','WA','WV','WI','WY');
		
		return in_array($value, $us_states);
  	}

  	public function valid_country($value, $params) 
  	{
  		$countries = array('USA', 'Canada');
		return in_array($value, $countries);
  	}
  	
  	public function valid_province_abbr($value, $params) 
  	{
  		// Canadian provinces
		$provinces = array('AB','BC','MB','NB','NL','NS','ON','PE','SK');
		
		return in_array($value, $provinces);
  	}  	  

  	public function valid_postal_code_format($value, $params)
  	{
  		$value = str_replace(' ', '', $value);
  		return (bool) preg_match('/^[^dfioquwz][0-9][^dfioqu][0-9][^dfioqu][0-9]$/i', $value);
  	}


	// =================
	// ! Phone numbers
	// =================

  	public function valid_phone($value, $params)
  	{
   		return (strlen($this->strip_non_numeric($value)) >= 10);
  	}
  	
	function valid_phone_us($value)
	{
		$format = 
			'/^
		    (?:                                 # Area Code
		        (?:                            
		            \(                          # Open Parentheses
		            (?=\d{3}\))                 # Lookahead.  Only if we have 3 digits and a closing parentheses
		        )?
		        (\d{3})                         # 3 Digit area code
		        (?:
		            (?<=\(\d{3})                # Closing Parentheses.  Lookbehind.
		            \)                          # Only if we have an open parentheses and 3 digits
		        )?
		        [\s.\/-]?                       # Optional Space Delimeter
		    )?
		    (\d{3})                             # 3 Digits
		    [\s\.\/-]?                          # Optional Space Delimeter
		    (\d{4})\s?                          # 4 Digits and an Optional following Space
		    (?:                                 # Extension
		        (?:                             # Lets look for some variation of extension
		            (?:
		                (?:e|x|ex|ext)\.?       # First, abbreviations, with an optional following period
		            |
		                extension               # Now just the whole word
		            )
		            \s?                         # Optionsal Following Space
		        )
		        (?=\d+)                         # This is the Lookahead.  Only accept that previous section IF it\'s followed by some digits.
		        (\d+)                           # Now grab the actual digits (the lookahead doesn\'t grab them)
		    )?                                  # The Extension is Optional
		    $/x';
	
		if (preg_match($format, $value, $parts))
		{
			if(!isset($parts[1]) || strlen($parts[1]) < 1 || !isset($parts[2]) || strlen($parts[2]) < 1 || !isset($parts[3]) || strlen($parts[3]) < 1)
			{
				return false;
			}
			
			$number = $parts[1].'-'.$parts[2].'-'.$parts[3];
			if(isset($parts[4]) && strlen($parts[4]) > 0)
			{
				$number .= ' ext. '.$parts[4];
			}
			return $number;
		}
		
		return false;
	}

  	
  	
  	// ===========
  	// ! Filters
	// ===========
	
	public function strip_non_numeric($value)
	{
		return preg_replace('/[^\d]/', '', $value);
	}

	public function humanize($value)
	{
		return trim(ucwords(strtr($value, '_', ' ')));
	}
	
  
  	// ===================== 
	// ! Customizations   
	// =====================
/*
	public function set_rules($field, $label = '', $rules = '')
	{
		if ( ! is_array($field))
		{
			$field = array(
				'field' => $field,
				'label' => $label,
				'rules' => $rules
			);
		}
		
		foreach ($field as $key => $value)
		{
		    if ( ! is_numeric($key))
		    {
		    	// Set the field name from the array key
		    	if ( ! isset($value['field']) || empty($value['field']))
		    	{
		    		$field[$key]['field'] = $key;
		    	}
		    	
		    	// Set the field label from a humanized array key
		    	if ( ! isset($value['label']) || empty($value['label']))
		    	{
		    		$field[$key]['label'] = $this->humanize($key);
		    	}
		    }
		    elseif ( ! isset($value['label']) && isset($value['field']))
		    {
		    	// Set the field label from a humanized field name
		    	$field[$key]['label'] = $this->humanize($key);
		    }
		}
		
		return parent::set_rules($field);
	}
*/
  	public function set_errors($fields = false)
	{
		if ($fields && is_array($fields))
		{
			foreach($fields as $key => $val)
			{
				$this->_error_array[$key] = $val;
				$this->_field_data[$key]['error'] = $val;
			}
		}     
	}
	
	public function get_errors()
	{
		return $this->_error_array;
	}
}

/**
 * Form Error Class
 *
 * Returns the error class name for a specific form field. This is a helper for the
 * form validation class.
 *
 * @access	public
 * @param	string
 * @param	string
 * @return	string
 */
function form_error_class($field = '', $class = 'error')
{
	if (FALSE === ($OBJ =& _get_validation_object()))
	{
		return '';
	}
	
	return $OBJ->error($field) ? $class : '';
}


/* End of file BMI_Form_validation.php */
/* Location: ./application/libraries/BMI_Form_validation.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->helper('form');
		if($this->input->get('video') && $this->input->get('video') == 'true')
			$data['video'] = TRUE;
		else
			$data['video'] = FALSE;
		$this->load->view('welcome_message', $data);
	}	

	public function german()
	{
		$this->load->helper('form');
		$this->load->view('german_landing');
	}
	

	public function rules()
	{
		$country_array = array('us', 'uk', 'au', 'gm');
		if($this->uri->segment(2) && in_array($this->uri->segment(2), $country_array)):
			$data['country'] = $this->uri->segment(2);
		else:
			$data['country'] = 'us';
		endif;
		$this->load->view('official_rules_'.$data['country'], $data);
	}

	// entry form post for german entries
	public function post_entry()
	{
		$this->load->library('form_validation');
		$this->load->database();

		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'rtrim|equired|valid_email');
		$this->form_validation->set_rules('dob', 'Date of birth', 'required|trim');
		$this->form_validation->set_rules('caption', 'Caption', 'required');
		$this->form_validation->set_rules('official_rules', 'Official Rules', 'required');
		$this->form_validation->set_rules('optin', 'Optin', '');

		if ($this->form_validation->run() == FALSE)
		{

			// $ajax_data = array('success' => false, 'error' => 'There was an error, please try again');

		} else {

			if($this->already_entered($this->input->post('email')))
			{
				exit(json_encode(array('success' => false, 'error' => 'Du hast bereits teilgenommen.')));
			}


			if($this->input->post('dob') == "")
			{
				$dob = "00/00/0000";
			} else {
				$dob = date('Y-m-d', strtotime(str_replace('-', '/', $this->input->post('dob'))));
			}

			$data = array(
				'first_name' 		=>	$this->input->post('first_name'),
				'last_name'			=>	$this->input->post('last_name'),
				'email'				=>	$this->input->post('email'),
				'dob'				=>	$dob,
				'caption'			=> 	$this->input->post('caption'),
				'optin'				=>	$this->input->post('optin'),
				'official_rules'	=>	$this->input->post('official_rules'),
				'created_at' => date('Y-m-d H:i:s'),
			);

			if($this->db->insert('entries', $data))
			{
			 	$ajax_data = array('success' => true);
			} else {
			 	$ajax_data = array('success' => false, 'error' => 'There was an error, please try again');
			}

	        exit(json_encode($ajax_data));
		}

	}

	public function already_entered($email = '')
	{
		$this->load->database();
		$sql = 'SELECT COUNT(id) AS cnt FROM entries WHERE email = ?';
		return $this->db->query($sql, array($email))->row()->cnt > 0;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Returns the current protocol.
function current_protocol()
{
	return (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS'])) ? 'https://' : 'http://';
}

// Returns the cdn URL.
function cdn_url($uri = '', $append_cache_buster = TRUE)
{
	$CI =& get_instance();

	// Build URL
	$cdn_hostname = $CI->config->item('cdn_hostname');
	$url = NULL;
	if($cdn_hostname && isset($cdn_hostname[ENVIRONMENT]))
		$url = rtrim(current_protocol().$cdn_hostname[ENVIRONMENT], '/').'/'.ltrim($uri, '/');
	else
		$url = $CI->config->base_url($uri);

	// If no cache buster required, then return.
	if (!$append_cache_buster)
		return $url;

	// Append cache buster.
	if (stripos($url, '?') === FALSE)
		return $url."?v=".$CI->config->item('app_version');
	else
		return rtrim($url, '&')."&v=".$CI->config->item('app_version');

	return $url;
}
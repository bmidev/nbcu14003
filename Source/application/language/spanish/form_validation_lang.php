<?php

// ! CodeIgniter validation error messages
$lang['required']			= "El %s campo es obligatorio.";
$lang['isset']				= "The %s field must have a value.";
$lang['valid_email']		= "The %s field must contain a valid email address.";
$lang['valid_emails']		= "El %s campo debe contener una direcci&oacute;n de correo electr&oacute;nico v&aacute;lida.";
$lang['valid_url']			= "The %s field must contain a valid URL.";
$lang['valid_ip']			= "The %s field must contain a valid IP.";
$lang['min_length']			= "The %s field must be at least %s characters in length.";
$lang['max_length']			= "El campo %s no puede exceder %s caracteres de longitud.";
$lang['exact_length']		= "El %s del campo debe ser exactamente %s caracteres de longitud.";
$lang['alpha']				= "The %s field may only contain alphabetical characters.";
$lang['alpha_numeric']		= "The %s field may only contain alpha-numeric characters.";
$lang['alpha_dash']			= "The %s field may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= "El %s campo debe contener s&oacute;lo n&uacute;meros.";
$lang['is_numeric']			= "The %s field must contain only numeric characters.";
$lang['integer']			= "The %s field must contain an integer.";
$lang['regex_match']		= "The %s field is not in the correct format.";
$lang['matches']			= "El %s no coincide con el campo %s.";
$lang['is_unique'] 			= "El %s campo debe contener un valor &uacute;nico.";
$lang['is_natural']			= "The %s field must contain only positive numbers.";
$lang['is_natural_no_zero']	= "The %s field must contain a number greater than zero.";
$lang['decimal']			= "The %s field must contain a decimal number.";
$lang['less_than']			= "The %s field must contain a number less than %s.";
$lang['greater_than']		= "The %s field must contain a number greater than %s.";

// ! BMI error messages
$lang['valid_name']			= '%s contiene caracteres no v&aacute;lidos.';
$lang['agree_to_rules']		= 'You must agree to the %s.';
$lang['exists']				= 'No v&aacute;lido %s.';
$lang['in_1']				= 'Invalid %s, must be {a}.';
$lang['in_2']				= 'Invalid %s, must be either {a} or {b}.';
$lang['in_n']				= 'Invalid %s, must be one of the following: ';
$lang['valid_date']			= 'Invalid %s.';
$lang['valid_date_multipart'] = 'Invalid fecha.';
$lang['valid_age']			= 'Usted debe ser al menos {age} a&ntilde;os de edad para participar.';
$lang['valid_state_abbr']	= 'A valid %s is required.';
$lang['valid_country']		= 'A valid %s is required.';
$lang['valid_province_abbr'] = 'A valid %s is required.';
$lang['valid_postal_code_format'] = 'A valid %s is required.';
$lang['valid_phone']		= 'A valid %s is required.';
$lang['valid_phone_us']		= 'A valid U.S. %s is required.';
$lang['valid_recaptcha']	= 'Invalid %s, please try again.';


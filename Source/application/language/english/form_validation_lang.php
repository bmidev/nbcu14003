<?php

// ! CodeIgniter validation error messages
$lang['required']			= "The %s field is required.";
$lang['isset']				= "The %s field must have a value.";
$lang['valid_email']		= "The %s field must contain a valid email address.";
$lang['valid_emails']		= "The %s field must contain all valid email addresses.";
$lang['valid_url']			= "The %s field must contain a valid URL.";
$lang['valid_ip']			= "The %s field must contain a valid IP.";
$lang['min_length']			= "The %s field must be at least %s characters in length.";
$lang['max_length']			= "The %s field can not exceed %s characters in length.";
$lang['exact_length']		= "The %s field must be exactly %s characters in length.";
$lang['alpha']				= "The %s field may only contain alphabetical characters.";
$lang['alpha_numeric']		= "The %s field may only contain alpha-numeric characters.";
$lang['alpha_dash']			= "The %s field may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= "The %s field must contain only numbers.";
$lang['is_numeric']			= "The %s field must contain only numeric characters.";
$lang['integer']			= "The %s field must contain an integer.";
$lang['regex_match']		= "The %s field is not in the correct format.";
$lang['matches']			= "The %s field does not match the %s field.";
$lang['is_unique'] 			= "The %s field must contain a unique value.";
$lang['is_natural']			= "The %s field must contain only positive numbers.";
$lang['is_natural_no_zero']	= "The %s field must contain a number greater than zero.";
$lang['decimal']			= "The %s field must contain a decimal number.";
$lang['less_than']			= "The %s field must contain a number less than %s.";
$lang['greater_than']		= "The %s field must contain a number greater than %s.";

// ! BMI error messages
$lang['valid_name']			= '%s contains illegal characters.';
$lang['agree_to_rules']		= 'You must agree to the %s.';
$lang['exists']				= 'Sorry, that does not appear to be a valid entry code. Please double-check your code and try again.';
$lang['in_1']				= 'Invalid %s, must be {a}.';
$lang['in_2']				= 'Invalid %s, must be either {a} or {b}.';
$lang['in_n']				= 'Invalid %s, must be one of the following: ';
$lang['valid_date']			= 'Invalid %s.';
$lang['valid_date_multipart'] = 'Invalid date.';
$lang['valid_age']			= 'You must be at least {age} years old to participate.';
$lang['valid_state_abbr']	= 'A valid %s is required.';
$lang['valid_country']		= 'A valid %s is required.';
$lang['valid_province_abbr'] = 'A valid %s is required.';
$lang['valid_postal_code_format'] = 'A valid %s is required.';
$lang['valid_phone']		= 'A valid %s is required.';
$lang['valid_phone_us']		= 'A valid U.S. %s is required.';
$lang['valid_recaptcha']	= 'Invalid %s, please try again.';


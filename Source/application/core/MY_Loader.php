<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";

class MY_Loader extends MX_Loader
{
	public function initialize($controller = NULL)
	{
		if (is_a($controller, 'MX_Controller'))
		{	
			/* reference to the module controller */
			$this->controller = $controller;
			
			/* references to ci loader variables */
			foreach (get_class_vars('CI_Loader') as $var => $val) {
				if ($var != '_ci_ob_level') {
					$this->$var =& CI::$APP->load->$var;
				}
			}
		}
		else
		{
			// -- DO NOT REMOVE --
			$this->_initialize();
			// -- DO NOT REMOVE --
		}
		
		/* set the module name */
		$this->_module = CI::$APP->router->fetch_module();
		
		/* add this module path to the loader variables */
		$this->_add_module_paths($this->_module);
	}

	/**
	 * Initialize the Loader
	 *
	 * This method is called once in CI_Controller.
	 *
	 * @param 	array
	 * @return 	object
	 */
	protected function _initialize()
	{
		$this->_ci_classes = array();
		$this->_ci_loaded_files = array();
		$this->_ci_models = array();
		$this->_base_classes =& is_loaded();

		// -- DO NOT REMOVE --
		$this->ci_autoloader();
		// -- DO NOT REMOVE --

		return $this;
	}


	/**
	 * Autoloader
	 *
	 * The config/autoload.php file contains an array that permits sub-systems,
	 * libraries, and helpers to be loaded automatically.
	 *
	 * @param	array
	 * @return	void
	 */
	protected function ci_autoloader()
	{
		if (defined('ENVIRONMENT') AND file_exists(APPPATH.'config/'.ENVIRONMENT.'/autoload.php'))
		{
			include(APPPATH.'config/'.ENVIRONMENT.'/autoload.php');
		}
		else
		{
			include(APPPATH.'config/autoload.php');
		}

		if ( ! isset($autoload))
		{
			return FALSE;
		}

		// Autoload packages
		if (isset($autoload['packages']))
		{
			foreach ($autoload['packages'] as $package_path)
			{
				$this->add_package_path($package_path);
			}
		}

		// Load any custom config file
		if (count($autoload['config']) > 0)
		{
			$CI =& get_instance();
			foreach ($autoload['config'] as $key => $val)
			{
				$CI->config->load($val);
			}
		}

		// Autoload helpers and languages
		foreach (array('helper', 'language') as $type)
		{
			if (isset($autoload[$type]) AND count($autoload[$type]) > 0)
			{
				$this->$type($autoload[$type]);
			}
		}


		// -- DO NOT REMOVE --
		
		// Autoload sparks
		if (isset($autoload['sparks'])) {
			foreach ($autoload['sparks'] as $config) {
				$this->sparks($config);
			}
		}
		
		// Autoload entities
		if (isset($autoload['entities'])) {
			foreach ($autoload['entities'] as $config) {
				$this->entity($config);
			}
		}
		
		// -- DO NOT REMOVE --
		

		// A little tweak to remain backward compatible
		// The $autoload['core'] item was deprecated
		if ( ! isset($autoload['libraries']) AND isset($autoload['core']))
		{
			$autoload['libraries'] = $autoload['core'];
		}
		
		// Load libraries
		if (isset($autoload['libraries']) AND count($autoload['libraries']) > 0)
		{
			// Load the database driver.
			if (in_array('database', $autoload['libraries']))
			{
				$this->database();
				$autoload['libraries'] = array_diff($autoload['libraries'], array('database'));
			}

			// Load all other libraries
			foreach ($autoload['libraries'] as $item)
			{
				$this->library($item);
			}
		}

		// Autoload models
		if (isset($autoload['model']))
		{
			$this->model($autoload['model']);
		}
	}
	
	/**
	 * Sparks loader
	 * http://getsparks.org
	 *
	 * @param $spark mixed
	 * @return void
	 */
	public function spark($spark)
	{
		// Make sure Modules::find() knows where to look for Sparks modules
		$sparkpath = dirname(realpath(APPPATH)).'/sparks/';
		if ( ! isset(Modules::$locations[$sparkpath]))
		{
			Modules::$locations[$sparkpath] = '../sparks/';
		}
		
		// Loading multiple sparks?
		if (is_array($spark))
		{
			foreach ($spark as $s)
			{
				$this->spark($s);
			}
			
			return;
		}
		
		$spark = trim($spark, '/');
		$path  = $sparkpath . $spark . '/';
		$parts = explode('/', $spark);
		$class = strtolower($parts[0]);
		
		$autoload_path = $path.'config/autoload'.EXT;
		
		if ($spark !== NULL && file_exists($autoload_path))
		{
			$current_module = $this->_module;
			$this->_module = $spark;
			$this->_autoloader(array());
			$this->_module = $current_module;
		}
		else
		{
			show_error("Spark $spark is missing a config/autoload.php file");
		}
	}
	
	public function sparks($sparks)
	{
		$this->spark($sparks);
	}
	
	/** Autoload module items **/
	public function _autoloader($autoload)
	{
		$path = FALSE;
		
		if ($this->_module)
		{
			list($path, $file) = Modules::find('constants', $this->_module, 'config/');	
			
			/* module constants file */
			if ($path != FALSE)
			{
				include_once $path.$file.EXT;
			}
					
			list($path, $file) = Modules::find('autoload', $this->_module, 'config/');
		
			/* module autoload file */
			if ($path != FALSE)
			{
				$autoload = array_merge(Modules::load_file($file, $path, 'autoload'), $autoload);
			}
		}
		
		/* nothing to do */
		if (count($autoload) == 0) return;
		
		/* autoload sparks */
		if (isset($autoload['sparks'])) {
			foreach ($autoload['sparks'] as $config) {
				$this->sparks($config);
			}
		}
		
		/* autoload entities */
		if (isset($autoload['entities'])) {
			foreach ($autoload['entities'] as $config) {
				$this->entity($config);
			}
		}
		
		/* autoload everything else */
		parent::_autoloader($autoload);
	}

	/**
	 * Entity Loader
	 *
	 * This function lets users load entities.
	 *
	 * @param $entities mixed
	 * @return void
	 */
	public function entity($entities)
	{
		if ( ! is_array($entities))
		{
			$entities = array($entities);
		}	
		
		foreach ($entities as $entity)
		{
			list($path, $_entity) = Modules::find($entity, $this->_module, 'entities/');
			if ($path !== FALSE)
			{
				Modules::load_file($_entity, $path);
			}
			else
			{
				$entity_path = APPPATH.'entities/' . str_replace('.php', '', $entity) . '.php';
			
				if ( ! file_exists($entity_path))
				{
					show_error('Unable to load the requested file: entities/' . $entity . '.php');
				}
			
				include_once $entity_path;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	 * Load Entities
	 *
	 * This is simply an alias to the above function in case the
	 * user has written the plural form of this function.
	 *
	 * @param	array
	 * @return	void
	 */	
	public function entities($entities)
	{
		$this->entity($entities);
	}
}
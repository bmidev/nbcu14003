<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="description" content="Enter the contest now for a chance to win a trip to the set of Pitch Perfect 2." />

	<meta property="og:image" content="<?php echo base_url(); ?>img/share_img.jpg" />

	<title>Pitch Perfect 2 Contest</title>
	<link rel="stylesheet" media="all" type="text/css" href="<?php echo site_url('css/default.css'); ?>" />
</head>
<body>
<div id="">
<center >

	<div id="body">
			<div class="bg">
				<img src="<?php echo site_url('img/bg.jpg'); ?>">
			</div>
		<?php if(isset($video) && $video == TRUE): ?>
			<div id="video">
				<img src= "<?php echo site_url('/img/video-bg.png'); ?>" usemap = "#urls" />
				<div id="videoplayer"></div>
				<script type="text/javascript" src="<?php echo site_url('js/youtube.js'); ?>"></script>
				<map name="urls">
					<area shape="rect" coords="30, 680, 265, 723" href="https://www.facebook.com/pitchperfectmovie" target="_blank" alt="USA Facebook Page" onclick="record_click('usa_fb_video');">
					<area shape="rect" coords="277, 680, 470, 723" href="https://www.facebook.com/PitchPerfectMovieUK" target="_blank" alt="UK Facebook Page" onclick="record_click('uk_fb_video');">
					<area shape="rect" coords="480, 680, 670, 723" href="https://www.facebook.com/pitchperfectmovieAU" target="_blank" alt="Australia Facebook Page" onclick="record_click('au_fb_video');">
					<area shape="rect" coords="680, 680, 875, 723" href=""  alt="Germany Facebook Page" onclick="record_click_german('gm_fb_video');" class="german-enter">
					
					<area shape="rect" coords="59, 737, 237, 750" href="<?php echo site_url('rules/us'); ?>" target="_blank" alt="USA Official Rules" onclick="record_click('usa_rules_video');">
					<area shape="rect" coords="299, 737, 445, 750" href="<?php echo site_url('rules/uk'); ?>" target="_blank" alt="UK Official Rules" onclick="record_click('uk_rules_video');">
					<area shape="rect" coords="520, 737, 630, 750" href="<?php echo site_url('rules/au'); ?>" target="_blank" alt="Australia Official Rules" onclick="record_click('au_rules_video');">
					<!--<area shape="rect" coords="722, 737, 836, 750" href="<?php echo site_url('rules/gm'); ?>" target="_blank" alt="Germany Official Rules" onclick="record_click('gm_rules_video');">-->
				</map>
			</div>
		<?php else: ?>
			<div id="image">
				<img src= "<?php echo site_url('/img/image-bg.png'); ?>" usemap = "#urls" />
				<map name="urls">
					<!--<area shape="rect" coords="136, 630, 370, 664" href="https://www.facebook.com/pitchperfectmovie" target="_blank" alt="USA Facebook Page" onclick="record_click('usa_fb_image');" border="1px">-->

					<area shape="rect" coords="277, 630, 468, 664" href="https://www.facebook.com/PitchPerfectMovieUK" target="_blank" alt="UK Facebook Page" onclick="record_click('uk_fb_image');">

					<area shape="rect" coords="481, 630, 674, 664" href="https://www.facebook.com/pitchperfectmovieAU" target="_blank" alt="Australia Facebook Page" onclick="record_click('au_fb_image');">

					<!--<area shape="rect" coords="690, 630, 885, 664" href="" alt="Germany Facebook Page" class="german-enter">-->
					
					<area shape="rect" coords="387, 711, 572, 732" href="<?php echo site_url('rules/us'); ?>" target="_blank" alt="USA Official Rules" onclick="record_click('usa_rules_image');">

					<area shape="rect" coords="294, 683, 460, 700" href="<?php echo site_url('rules/uk'); ?>" target="_blank" alt="UK Official Rules" onclick="record_click('uk_rules_image');">

					<area shape="rect" coords="514, 683, 638, 700" href="<?php echo site_url('rules/au'); ?>" target="_blank" alt="Australia Official Rules" onclick="record_click('au_rules_image');">

					<!--<area shape="rect" coords="725, 695, 845, 705" href="<?php echo site_url('rules/gm'); ?>" target="_blank" alt="Germany Official Rules" onclick="record_click('gm_rules_image');">-->
				</map>
			</div>
		<?php endif; ?>
	</div>

	<p class="footer">
		<img src= "<?php echo site_url('/img/footer.png'); ?>" usemap = "#footer" />
		<map name="footer">
			<area shape="rect" coords="587, 18, 654, 29" href="http://instagram.com/pitchperfectmovie" target="_blank" alt="Instagram" onclick="record_click('instagram');">
			<area shape="rect" coords="706, 18, 756, 29" href="https://twitter.com/PitchPerfect" target="_blank" alt="Twitter" onclick="record_click('twitter');">
			<area shape="rect" coords="810, 18, 856, 29" href="http://www.illtumblrforya.com/" target="_blank" alt="Tumblr" onclick="record_click('tumblr');">
			<area shape="rect" coords="1045, 18, 1115, 29" href="http://www.nbcuni.com/privacy/" target="_blank" alt="Privacy Policy" onclick="record_click('privacy_policy');">
			<area shape="rect" coords="1130, 18, 1195, 29" href="http://www.universalstudiosentertainment.com/about/terms-of-use.php" target="_blank" alt="Terms of Use" onclick="record_click('terms_of_use');">
		</map>
	</p>
</center>
</div>

<!-- GERMAN REGISTRATION MODAL-->
<div id="german-registration-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                <h1>Register to Win</h1>
            </div>
            <div class="modal-body">
		        <?php echo form_open('welcome/post_entry', 'id="german-registration-form"'); ?>
            	<div class="modal-body-left">
            		<div class="registration-form-promo-image"></div>

	                <div class="form-group">
		                <label for="caption" class="textarea-caption">Type out your caption below.*</label>
		                <br /><span class="character-limit">(140 character limit)</span>
		                <?php echo form_textarea('caption', set_value('caption'), 'class="form-control required"'); ?>
		            </div>
            	</div>

            	<div class="modal-body-right">

            		<span class="required-fields">*Required Fields</span>
            		<span class="required-fields-error">*Please fill out the required fields below</span>

            		<div class="modal-body-right-form">
		                <div class="form-group">
			                <label for="first_name" class="control-label">First Name*</label>
			                <?php echo form_input('first_name', set_value('first_name'), 'class="form-control required"'); ?>
			            </div>
		                <div class="form-group">
			                <label for="last_name" class="control-label">Last Name*</label>
			                <?php echo form_input('last_name', set_value('first_name'), 'class="form-control required"'); ?>
			            </div>
		                <div class="form-group">
			                <label for="email" class="control-label">Email*</label>
			                <?php echo form_input('email', set_value('email'), 'class="form-control required"'); ?>
			            </div>
		                <div class="form-group">
			                <label for="dob" class="control-label">Date of Birth*</label>
			                <?php echo form_input('dob', set_value('dob'), 'class="form-control required" id="dob"'); ?>
			            </div>

		                <div class="form-group official_rules">
		                	<?php echo form_checkbox('official_rules', 1, set_checkbox('official_rules'), 'class="form-control required"'); ?>

			                <label for="official_rules" class="checkbox-label">Offcial Rules*</label>
			            </div>

		                <div class="form-group optin">
			                <?php echo form_checkbox('optin', 1, set_checkbox('optin'), 'class="form-control"'); ?>

			                <label for="optin" class="checkbox-label">Get more info from Universal</label>
			            </div>

			            <?php echo form_submit('submit', 'Submit', 'id="submit-button"'); ?>
			        </div>

		        </div>
		        <?php echo form_close(); ?>

            </div><!-- /.modal-body -->
            <div class="modal-footer">
            </div><!-- /.modal-footer-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- GERMAN REGISTRATION MODAL-->

<!--<div class="modal fade hidden-print" id="dynamic-modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close refresh" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="center-block text-center">
                	<h1>Thank You</h1>

                	<p>Thank you for entering Pitch Perfect 2's Caption Contest!<br />The Grand Prize winner will be selected soon.</p>

                	<a href="" class="return-home-btn">Return Home</a>

                    <h3 id="dynamic-body-title"></h3>
                    <div id="dynamic-body-message"></div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade hidden-print" id="thanks-modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close refresh" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="center-block text-center">
                	<h1>Thank You</h1>

                	<p>Thank you for entering Pitch Perfect 2's Caption Contest!<br />The Grand Prize winner will be selected soon.</p>

                	<a href="" class="return-home-btn">Return Home</a>
                </div>
            </div>
        </div>
    </div>
</div>-->

<script type="text/javascript" src="<?php echo site_url('js/jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('js/default.js'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('js/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('js/jquery.inputmask.js'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('js/bootstrap.min.js'); ?>"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51789896-1', 'brandmovers.net');
  ga('send', 'pageview');

</script>
</body>
</html>
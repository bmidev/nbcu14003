<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="description" content="Man kann eine Reise ans Set von Pitch Perfect 2 gewinnen!! Mach mit!" />

	<meta property="og:image" content="<?php echo base_url(); ?>img/share_img.jpg" />

	<title>Pitch Perfect 2 Gewinnspiel</title>
	<link rel="stylesheet" media="all" type="text/css" href="<?php echo site_url('css/default.css'); ?>" />
</head>
<body>
<div id="">
<center >
	<div id="body">
		<div class="bg">
			<img src="<?php echo site_url('img/german-bg.jpg'); ?>">
		</div>
		
		<div id="german-registration">
			<!-- <img src= "<?php echo site_url('/img/image-bg.png'); ?>" usemap = "#urls" /> -->


	        <div class="modal-content">
	            <div class="modal-header">
	                <h1>Teilnehmen &amp; Gewinnen</h1>
	            </div>
	            <div class="modal-body">
			        <?php echo form_open('welcome/post_entry', 'id="german-registration-form"'); ?>
	            	<div class="modal-body-left">
	            		<div class="registration-form-promo-image"></div>

		                <div class="form-group">
			                <label for="caption" class="textarea-caption">Dein Text zum Bild*<br /><span class="character-limit">(max. 140 Zeichen)</span></label>
			                
			                <?php echo form_textarea('caption', set_value('caption'), 'class="form-control required"'); ?>
			            </div>
	            	</div>

	            	<div class="modal-body-right">

	            		<span class="required-fields">*Pflichtangaben</span>
	            		<span class="required-fields-error">*Bitte alle Felder ausfüllen</span>

	            		<div class="modal-body-right-form">

	            			<p class="german-reg-form-intro">Welcher Text passt deiner Meinung nach am besten zu diesem Bild. Sag es uns und du hast die Chance eine Reise ans Set von Pitch Perfect 2 zu gewinnen!!</p>

			                <div class="form-group">
				                <label for="first_name" class="control-label">Vorname*</label>
				                <?php echo form_input('first_name', set_value('first_name'), 'class="form-control required"'); ?>
				            </div>
			                <div class="form-group">
				                <label for="last_name" class="control-label">Nachname*</label>
				                <?php echo form_input('last_name', set_value('first_name'), 'class="form-control required"'); ?>
				            </div>
			                <div class="form-group">
				                <label for="email" class="control-label">E-Mail Adresse*</label>
				                <?php echo form_input('email', set_value('email'), 'class="form-control required"'); ?>
				            </div>
			                <div class="form-group">
				                <label for="dob" class="control-label">Geburtsdatum* (mm/dd/yyyy)</label>
				                <?php echo form_input('dob', set_value('dob'), 'class="form-control required" id="dob"'); ?>
				            </div>

			                <div class="form-group official_rules">
			                	<?php echo form_checkbox('official_rules', 1, set_checkbox('official_rules'), 'class="form-control required"'); ?>

				                <label for="official_rules" class="checkbox-label">Ich habe die <a href="/rules/gm" target="blank">Teilnahmebedingungen</a> gelesen und erkläre mich damit einverstanden*</label>
				            </div>

				            <?php echo form_submit('submit', 'Submit', 'id="submit-button"'); ?>
				        </div>

			        </div>
			        <?php echo form_close(); ?>

	            </div><!-- /.modal-body -->
	            <div class="modal-footer">
	            </div><!-- /.modal-footer-->
	        </div><!-- /.modal-content -->

		</div>
	</div>

	<p class="footer">
		<img src= "<?php echo site_url('/img/german_footer.png'); ?>" usemap = "#footer" />
		<map name="footer">
			<area shape="rect" coords="587, 18, 654, 29" href="http://instagram.com/pitchperfectmovie" target="_blank" alt="Instagram" onclick="record_click('instagram');">
			<area shape="rect" coords="706, 18, 756, 29" href="https://twitter.com/PitchPerfect" target="_blank" alt="Twitter" onclick="record_click('twitter');">
			<area shape="rect" coords="810, 18, 856, 29" href="http://www.illtumblrforya.com/" target="_blank" alt="Tumblr" onclick="record_click('tumblr');">
			<area shape="rect" coords="1006, 18, 1118, 39" href="http://upig.de/service/datenschutz.html" target="_blank" alt="Privacy Policy" onclick="record_click('gm_privacy_policy');">
			<area shape="rect" coords="1130, 18, 1195, 29" href="http://www.universalstudiosentertainment.com/about/terms-of-use.php" target="_blank" alt="Terms of Use" onclick="record_click('terms_of_use');">
		</map>
	</p>
</center>
</div>

<div class="modal fade hidden-print" id="thanks-modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close refresh" data-dismiss="modal" aria-hidden="true">&times;</button>
                <!--<h4 class="modal-title"></h4>-->
            </div>
            <div class="modal-body">
                <div class="center-block text-center">
                	<h1>Vielen Dank</h1>

                	<p>Vielen Dank für deine Teilnahme am Pitch Perfect
Gewinnspiel!<br />Der Gewinner wird in Kürze ausgewählt.</p>

					<a href="https://www.facebook.com/dialog/feed?app_id=1502361363326222&name=Pitch+Perfect+2+Gewinnspiel&description=Man+kann+eine+Reise+ans+Set+von+Pitch+Perfect+2+gewinnen!!+Mach+mit!.&picture=<?php echo base_url(); ?>img/share_img.jpg&link=<?php echo base_url(); ?>pp2germancontest%2F&redirect_uri=<?php echo base_url(); ?>pp2germancontest" target="_blank" class="share-on-facebook" onclick="record_click('gm_fb_share');">Pitch Perfect auf Facebook<img src="/img/facebook_share.png" title="Pitch Perfect auf Facebook"></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade hidden-print" id="dynamic-modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close refresh" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h1 class="modal-title"></h1>
            </div>
            <div class="modal-body">
                <div class="center-block text-center">
                	<!--<a href="" class="return-home-btn">Return Home</a>-->

                    <!--<h3 id="dynamic-body-title"></h3>
                    <div id="dynamic-body-message"></div>-->

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('js/jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('js/default.js'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('js/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('js/jquery.inputmask.js'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('js/bootstrap.min.js'); ?>"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51789896-1', 'brandmovers.net');
  ga('send', 'pageview');

</script>
</body>
</html>
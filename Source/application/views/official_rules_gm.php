<html>
<head>
	<title>OFFIZIELLE REGELN</title>
	<meta charset="utf-8">
	<link rel="stylesheet" media="all" type="text/css" href="<?php echo site_url('css/default.css'); ?>" />
</head>
<body>

<div class="official-rules">
<h1>"PITCH PERFECT 2" BILDBESCHRIFTUNGSWETTBEWERB<br />OFFIZIELLE REGELN</h1>

<h3>KEINE ANMELDEGEBÜHR UND KEIN FINANZIELLER EINSATZ. ZUM MITMACHEN ODER GEWINNEN DIESES WETTBEWERBS IST KEIN KAUF ERFORDERLICH.  EIN KAUF ERHÖHT NICHT IHRE GEWINNCHANCEN.</h3>

<ol>
	<li><strong>SPONSOR:</strong> Universal Pictures International Germany GMBH, Hahnstrasse 31-35, 60528 Frankfurt, Deutschland.</li>

	<li><strong>TEILNAHMEBERECHTIGUNG:</strong> "Pitch Perfect 2", der größte Bildbeschriftungswettbewerb (der "Wettbewerb"), ist nun angelaufen und wird nur (a) Einwohnern Deutschlands angeboten, die einen gültigen Aufenthaltsstatus haben, (b) zum Zeitpunkt ihrer Teilnahme mindestens  18 Jahre alt sind („Teilnehmer“).  Personen, die nach dem Rechtssystem ihres Wohnsitzes als minderjährig gelten („Minderjährige“), müssen die Erlaubnis ihrer Eltern oder ihres Vormunds haben, um sich zum Wettbewerb anmelden zu können.  Angestellte, Amtsträger und Führungskräfte des Sponsors und seiner eventuellen Muttergesellschaften, Tochtergesellschaften oder angeschlossenen Gesellschaften oder von Werbe-, Marketing- und PR-Agenturen (sowie deren abhängige und enge Familienmitglieder [Kinder, Ehepartner, Eltern, Geschwister und deren Ehepartner, unabhängig von ihrem Wohnsitz] und Personen, die im selben Haushalt leben, unabhängig von einem verwandtschaftlichen Verhältnis) sind nicht teilnahmeberechtigt.</li>

	<li><strong>DURCHFÜHRUNG DER TEILNAHME:</strong> Die Teilnahme am Wettbewerb erfolgt freiwillig und ist kostenlos. Dieser Wettbewerb beginnt am 23. Juni 2014 um 10:00:00 Uhr mitteleuropäischer Sommerzeit („MESZ“) und endet am 29. Juni 2014 um 23:59:59 Uhr MESZ („Wettbewerbszeitraum“). Um sich zum Wettbewerb anzumelden, gehen Sie während des Wettbewerbszeitraums zu <a href="https://pitchperfect2.brandmovers.net/pp2germancontest" target="_blank">https://pitchperfect2.brandmovers.net/pp2germancontest</a> („Website”) und tun Sie Folgendes: (a) Füllen Sie ein ein offizielles Anmeldeformular mit allen nötigen Informationen aus, unter anderem Ihrem vollständigen Namen, Ihrer E-Mail-Adresse und Ihrem Geburtsdatum; (b) erklären Sie auf elektronischem Weg Ihr Einverständnis mit den offiziellen Regeln; (c) senden Sie Ihre originelle Bildbeschriftung zum angebotenen Foto ein („Eingabeformular”) und Ihr Beitrag wird automatisch in den Wettbewerb aufgenommen („Beitrag” oder „Beiträge”). Alle Beiträge müssen bis spätestens 29. Juni 2014, 23:59:59 Uhr MESZ eingehen.

	<p>Der Sponsor übernimmt keine Verantwortung für verloren gegangene, verspätete, unvollständige, ungültige, unverständliche, unleserliche oder fehlgeleitete Beiträge. Diese werden disqualifiziert.  Die Uhr in der Datenbank des Sponsors stellt die offizielle Zeitvorgabe für diesen Wettbewerb dar.</p>

	<p>Der Sponsor hat das Recht, alle Beiträge zu disqualifizieren, die nicht diesen offiziellen Regeln entsprechen.</p>

	<p>Begrenzung: Jeder Teilnehmer darf sich während des Wettbewerbszeitraums nur einmal (1-mal) zum Wettbewerb anmelden.  Mehrfache Beiträge von demselben Teilnehmer sind nicht gestattet. Ein Beitrag darf nicht von mehreren Teilnehmern gleichzeitig kommen. Jeder Versuch eines Teilnehmers, mehr als die festgesetzte Anzahl von Beiträgen einzusenden, indem er mehrere/verschiedene E-Mail-Adressen, Identitäten, Registrierungen oder andere Methoden nutzt, macht die Beiträge dieses Teilnehmers ungültig und der Teilnehmer kann nach Ermessen des Sponsors disqualifiziert werden.</p>

	<p>Der Beitrag ist vom Wettbewerbsteilnehmer selbst vorzulegen.  Beiträge, die von anderen natürlichen oder juristischen Personen eingereicht wurden bzw. von einer anderen Website stammen oder von einer anderen E-Mail-Adresse kommen - wozu unter anderem kommerzielle Websites gehören, die Wettbewerbsabonnements oder Anmeldedienste anbieten, werden für ungültig erklärt und von diesem Wettbewerb disqualifiziert.  Manipulationen beim Teilnahmevorgang, wie die oben beschriebenen, oder beim Ablauf des Wettbewerbs, einschließlich der Nutzung von Gerätschaften zur Automatisierung des Teilnahmevorgangs, sind verboten und alle Beiträge, die auf eine solche Weise eingehen, sind nichtig.  Im Fall von Streitigkeiten darüber, wer einen Beitrag eingesandt hat, gilt der zum Zeitpunkt der Einsendung befugte Inhaber des E-Mail-Kontos, das im Zusammenhang mit der Einsendung angegeben wurde, als der Wettbewerbsteilnehmer. Als „befugter Inhaber des E-Mail-Kontos” gilt definitionsgemäß die natürliche Person, die von einem Internetzugangsanbieter, einem Online-Dienstleistungsanbieter oder einer anderen Organisation (z.B. einem Wirtschaftsunternehmen oder einer Bildungseinrichtung), die für die Zuteilung von E-Mail-Adressen für die mit der angegebenen E-Mail-Adresse verbundene Domäne zuständig ist, eine E-Mail-Adresse zugeteilt bekommen hat.</p></li>

	<li><strong>ANFORDERUNGEN AN/BESCHRÄNKUNGEN FÜR EINSENDUNGEN:</strong> Durch das Einsenden eines Beitrags gewährleistet jeder Teilnehmer, (i) dass es sich um einen Originalbeitrag des Teilnehmers handelt und (ii) dass er dem Sponsor und seinen Vertretern das unbefristete, unbeschränkte, nicht ausschließliche, weltweite, übertragbare und unwiderrufliche Recht erteilt, den Beitrag und alle seine Elemente, einschließlich aller Namen und Ähnlichkeiten, auf beliebige Weise und in allen beliebigen, heute bekannten oder erst in Zukunft entwickelten Medien zu modifizieren, abgeleitete Werke davon zu schaffen, sie zu bearbeiten, zu veröffentlichen oder anderweitig zu nutzen, ohne dass eine weitere Entlohnung anfällt oder eine Benachrichtigung oder Genehmigung nötig ist. Dazu gehört die Lizenz auf alle Urheberrechte, Handelsmarken oder andere Rechte, die Sie an dem Inhalt und dem Beitrag halten könnten.   Um teilnahmeberechtigt zu sein und als Beitrag zu gelten, darf der Beitrag (1) bis dahin von keiner dritten Partei eingesandt worden sein, auch nicht im Rahmen einer anderen Werbeaktion; (2) nicht vertraulich zu sein; (3) rechtmäßig erstellt worden zu sein; (4) keine Urheberrechte, Handelsmarken, Rechte auf Privatsphäre, Publizität oder andere geistige Eigentumsrechte oder andere juristische oder moralische Rechte einer natürlichen oder juristischen Person zu verletzen; (5) nicht obszön, anstößig, unangemessen zu sein oder dem Image des Sponsors zu schaden (was vom Sponsor nach seinem alleinigen Ermessen beurteilt wird); (6) der Beitrag darf keine persönliche Identifikation zu enthalten, wie etwa das Nummernschild des eigenen Autos, Namen von Personen, E-Mail-Adressen oder Straßenadressen, die auf irgendeine Weise die Rechte einer dritten Partei verletzen; (7) keine Rechte von Dritten zu verletzen, einschließlich der Rechte auf Copyright, Handelsmarken, Privatsphäre und Publizität.  Das bedeutet, dass der Beitrag unter anderem nicht Folgendes enthalten darf: (i) Material von anderen Personen, das dem Urheberschutz unterliegt (einschließlich Musik, Fotos, Skulpturen, Gemälden und anderen Kunstwerken oder Abbildungen, die auf Websites, im Fernsehen, in Filmen oder anderen Medien veröffentlicht wurden); (ii) identifizierbare Namen, Kennzeichnungen, Handelsmarken, Logos oder Handelsaufmachungen, die Dritten gehören (z. B. Logos oder Handelsmarken auf Kleidungsstücken); oder (iii) Materialien, die Namen, Ähnlichkeiten, Fotos oder andere Einzelheiten enthalten, die eine Person identifizieren könnten, einschließlich von Prominenten oder Persönlichkeiten aus dem öffentlichen Leben, lebend oder verstorben, ohne deren vorherige schriftliche Erlaubnis. (8) Beiträge sollten für keine Marke und kein Produkt jedweder Art werben oder es ohne Erlaubnis anbieten und (9) Beiträge dürfen nicht den Sponsor in seiner Ehre verletzen, falsche Aussagen über ihn machen oder abfällige Bemerkungen über ihn oder seine Produkte oder über andere Menschen, Produkte oder Unternehmen enthalten. (10) Sie dürfen keine Gewalt und keine eindeutig sexuellen Andeutungen beinhalten oder auf andere Weise eine Gruppe von Menschen oder eine Gesellschaftsschicht angreifen oder schlechtmachen oder ein schlechtes Licht auf eine der werbenden Parteien werfen, jemanden diffamieren oder die Rechte von Personen auf Privatsphäre oder Publizität verletzen.</li>

	<li><strong>BEWERTUNGSKRITERIEN FÜR DEN WETTBEWERB:</strong> Alle gültigen Beiträge, die beim Sponsor eingehen, werden vom 30. Juni bis einschließlich 1. Juli 2014 von einer Gruppe von Richtern bewertet („Jury“), wobei die folgenden Bewertungskriterien und Prozentzahlen zur Anwendung kommen: demonstrierte Innovation/Kreativität (40 %), Originalität (35 %), Relevanz der Bildbeschriftung für das „Pitch Perfect 2”-Bild (25 %). Die Beschlüsse der Jury sind in allen Angelegenheiten, die den Wettbewerb betreffen, endgültig und bindend. Der Teilnehmer mit der höchsten Gesamtpunktzahl ist der Gewinner des Ersten Preises. Falls es zu einem Punktegleichstand bei Beiträgen kommt, gilt der Beitrag mit der höchsten Punktzahl für die Relevanz der Bildbeschriftung für das „Pitch Perfect 2”-Bild als der Gewinner.

	<p><strong>BENACHRICHTIGUNG DER GEWINNER:</strong> Ein (1) potentieller Gewinner des Ersten Preises (der „potentielle Gewinner“) wird am oder um den 1. Juli 2014 von der Jury bestimmt. Der potentielle Gewinner wird über die E-Mail-Adresse, die er im Anmeldeformular angegeben hat, benachrichtigt. Der potentielle Gewinner wird sich mit dem Sponsor innerhalb von 24 Stunden, nachdem er die Benachrichtigung erhalten hat, per E-Mail in Verbindung setzen müssen, damit der Verifizierungsprozess des Gewinners beginnen kann. Der potentielle Gewinner wird dann innerhalb von achtundvierzig (48) Stunden nach der Gewinnbenachrichtigung per E-Mail eine Erklärung zur Eignung und Publizität, wie sie hier beigefügt ist, zugesandt bekommen, bevor ihm der Preis verliehen wird. In der Erklärung zur Eignung und Publizität wird der potentielle Gewinner gebeten zu bestätigen, dass er (i) alle Eignungskriterien erfüllt, die in diesen offiziellen Regeln angeführt sind, (ii) die offiziellen Regeln eingehalten hat und einhalten wird, (iii) dem Sponsor die Rechte an dem Beitrag verleiht, die im obigen Abschnitt 4 (ii) sowie im folgenden Abschnitt 8 beschrieben sind.  Der potentielle Gewinner muss alle Eignungsanforderungen erfüllen und die Erklärung zur Eignung und Publizität unterschreiben sowie dafür sorgen, dass der Gast die hier beigefügte Freigabe des Reisebegleiters unterschreibt. Alle diese Dokumente müssen ausgefüllt, unterschrieben und innerhalb von achtundvierzig (48) Stunden nach Ausstellung zurückgesandt werden (damit sichergestellt wird, dass die Reise termingerecht angetreten werden kann); ansonsten verfällt der Preis und kann einem alternativen Gewinner verliehen werden, wenn es die Zeit erlaubt. Wenn eine Preisbenachrichtigung oder versuchte Benachrichtigung als unzustellbar zurückkommt, verfällt der Preis und wird einem alternativen Gewinner verliehen.</p>

	<p>Zu einer Disqualifikation, einem Verfall des Gewinns und der Auswahl eines anderen Gewinners kann es aus folgenden Gründen kommen:  [1] wenn der potentielle Gewinner nicht innerhalb von  vierundzwanzig (24) Stunden nach Absenden der Benachrichtigung antwortet, [2] wenn der potentielle Gewinner nicht die Erklärung zur Eignung / Publizitätsfreigabe nicht innerhalb von achtundvierzig (48) Stunden zurückschickt, nachdem sie ihm per E-Mail zugesandt wurde, [3]wenn der potentielle Gewinner dem Sponsor bis zum 2. Juli 2014 keinen zufriedenstellenden Alters-, Identitäts- und Wohnsitznachweis vorlegen kann und [4] wenn eine sonstige Nichterfüllung dieser offiziellen Regeln vorliegt. Bei Verfall eines Preises liegt es im Ermessen des Sponsors, den verfallenen Preis nicht zu verleihen oder ihn einem alternativen Gewinner zu verleihen, d. h. demjenigen, dessen Beitrag die nächsthöhere Punktzahl erreicht hat.</p></li>

	<li><strong>PREISE UND DEREN UNGEFÄHRE WERTE: (1) Hauptgewinn:</strong> eine Reise für den Gewinner und einen (1) Gast (der „Gast“) nach Baton Rouge, LA („Ort des Hautgewinns“), zu einem Termin, der zwischen dem 8. Juli und dem 11. Juli 2014 festgelegt wird, um das Set von Pitch Perfect 2 zu besuchen (die „Reise“ oder der „Preis“). Die Reise beinhaltet: (a) Hin- und Rückflug in der Touristenklasse für den Gewinner und den Gast von einem größeren Flughafen in der Nähe des Wohnsitzes des Gewinners zu einem Flughafen nahe dem Ort des Hauptgewinns und zurück (beide Flughäfen werden vom Sponsor bestimmt und Zwischenlandungen sind möglich); (b) zwei (2) aufeinanderfolgende Übernachtungen in einem Hotel nach Wahl des Sponsors (ein Standardzimmer mit Doppelbelegung, nur Zimmer und Steuern); (c) Zubringerfahrten zwischen dem Flughafen und dem Hotel sowie zum Set des Pitch Perfect 2; (d) $150 pro Nacht zur Verwendung für Speisen und Getränke im Hotel und (e) einen Besuch des Sets von Pitch Perfect 2.

	<p>GESAMTWERT des Hauptpreises: 5.300,00 US-Dollar</p>

	<p>Die Organisation der Reise und der Unterkunft erfolgt durch die Reiseagentur des Sponsors nach den Vorgaben des Sponsors. Die Reisedokumente und sonstige Unterlagen werden an die E-Mail-Adresse des Gewinners geschickt, die im Anmeldeformular angegeben wurde. Der Gewinner muss alle zusätzlichen Dokumente unterschreiben, die ihm der Sponsor zukommen lässt und die notwendig sind, damit die Reise in dem vorgegebenen Zeitraum unternommen werden kann. Wenn dies der Gewinner nicht tut, könnte dies dazu führen, dass der Preis verfällt.</p>

	<p>Der Sponsor sollte nicht als Reiseveranstalter betrachtet werden und er trägt nicht die Verantwortung, die nach dem Gesetz ein Reiseveranstalter trägt, wenn es zu Stornierungen, Verzögerungen oder irgendwelchen sonstigen Handlungen oder Unterlassungen von Seiten der Leistungserbringer/Veranstalter, Hotels oder anderer Personen kommt, die in diesem Zusammenhang irgendwelche Dienstleistungen erbringen oder Unterkünfte anbieten.</p>

	<p><strong>DIE REISE MUSS IN DER ZEIT VOM 8. JULI 2014 BIS 11. JULI 2014 UNTERNOMMEN WERDEN..</strong> Der tatsächliche Wert des Hauptpreises kann je nach Abflugort und Schwankungen bei den Transportkosten variieren. Eventuelle Differenzen zwischen dem geschätzten Wert und dem tatsächlichen Wert des Preises werden nicht erstattet. Gewinner und Gast müssen zusammen reisen und müssen die notwendigen Reisedokumente besitzen, z. B. einen gültigen Reisepass und ein Reisevisum. Kosten und Auslagen, die mit der Annahme und Ausführung des Preises verbunden sind und die hier nicht spezifisch angeführt wurden, liegen in der alleinigen Verantwortung des Gewinners. Dazu gehören unter anderem zusätzliche Landtransporte, Mahlzeiten, Getränke, Wäschereinigung, Versicherungen, Gepäckgebühren, Einkaufswaren, Parkgebühren, Zimmerservice, Servicegebühren, Souvenirs, Kurbadedienste, Telefongespräche, Bundes- und Gemeindesteuern, Flughafen- und Zollgebühren sowie Trinkgelder. Wenn der Gewinner minderjährig ist, wird der Hauptpreis an einen Elternteil oder Vormund verliehen (der „Elternteil“).  Der Elternteil muss den Minderjährigen auf der Reise begleiten und der Minderjährige nimmt dabei die Stelle des Gastes ein.  Wenn der Gast minderjährig ist, muss er mit seinem Erziehungsberechtigten reisen, und dieser muss der Gewinner sein. Der Preis kann nicht  in Bargeld ausgezahlt werden und ist nicht übertragbar. Die Elemente des Hauptpreises können nicht voneinander getrennt werden. Preiselemente können anderen Beschränkungen unterliegen. Es kann sein, dass sie nicht mit anderen Angeboten kombiniert werden können, und es kann sein, dass die Reise nicht für Vielfliegerrabatte angerechnet wird. Der Sponsor wird die Reise und das Hotel für den Gewinner organisieren, wobei angemessene Wünsche des Gewinners berücksichtigt werden. Der Mitreisende muss am Abflugdatum mindestens 18 Jahre alt sein, es sei denn, es handelt sich um ein minderjähriges Kind des Gewinners. Der Preis verfällt, wenn der Gewinner nicht in der Lage sein sollte, die Reise zu den vorgegebenen Daten und Bedingungen anzutreten.</p></li>

	<li><strong>NUTZUNG VON PERSONENBEZOGENEN DATEN:</strong> Alle Informationen, die Sie im Zusammenhang mit diesem Wettbewerb zur Verfügung stellen, fallen unter die Datenschutzbestimmungen des Sponsors, die unter <a href="http://upig.de/service/datenschutz.html" target="_blank">http://upig.de/service/datenschutz.html</a> eingesehen werden können. Die von Ihnen verfügbar gemachten Informationen werden von Brandmovers Inc.  („Brandmovers”), einem Datenverarbeitungsunternehmen in den USA, verarbeitet. Durch die Anmeldung zu diesem Wettbewerb erklärt sich jeder Teilnehmer einverstanden, dass der Sponsor das Recht hat, den Teilnehmer über die E-Mai-Adresse lzu kontaktieren, die dieser im Anmeldeformular zur Verfügung gestellt hat, um diesen Wettbewerb zu verwalten und abzuschließen.</li>

	<li><strong>DATENSCHUTZ/DATENERFASSUNG:</strong>  The Promoter collects personal information in order to conduct the Contest and may, for this purpose, disclose such information to third parties, including, but not limited to, the Administrator and Prize suppliers. Entry is conditional on providing this information.  Unless otherwise advised, the Promoter may also use the information for promotional, marketing and publicity purposes.  Entrants should direct any request to access, update or correct information to the Promoter.  Any information provided by you in connection with this Contest is subject to Promoter's privacy policy located at <a href="http://upig.de/service/datenschutz.html" target="_blank">http://upig.de/service/datenschutz.html</a>.  The Promoter's privacy policy contains information about how an Entrant may access or correct personal information held about them or complain about a breach of their privacy.  By entering this Contest, each Entrant consents to the Promoter or Administrator contacting the Entrant by messaging Entrant's Facebook account in order to administer and fulfill this Contest.</li>

	<li><strong>HAFTUNGSBEGRENZUNG:</strong> Durch diese offiziellen Regeln wird die gesetzliche Haftpflicht des Sponsors für Todesfälle oder Personenschäden, betrügerische Falschaussagen, beabsichtigtes Fehlverhalten, grobe Fahrlässigkeit, Produkthaftung, gegebene Garantien oder andere Haftungen, die laut Gesetz nicht ausgeschlossen oder eingeschränkt werden dürfen, in keiner Weise beschränkt oder ausgeschlossen. 

	<p>Mit Ausnahme der Haftung nach dem vorstehenden Absatz wird die gesetzliche Haftpflicht des Sponsors für leichte Fahrlässigkeit folgendermaßen beschränkt:</p>

	<ul>
		<li>Die gesetzliche Haftungsverpflichtung für Verluste und Schäden, die durch leicht fahrlässige Verletzung einer wesentlichen vertraglichen Verpflichtung verursacht werden, also einer vertraglichen Verpflichtung, deren Erfüllung für die ordnungsgemäße Durchführung der offiziellen Regeln notwendig ist und deren Verletzung den Zweck der offiziellen Regeln gefährdet und auf deren Erfüllung sich der Teilnehmer normalerweise verlässt, wird auf die Schadenshöhe begrenzt, die zu dem Zeitpunkt, an dem die Parteien die offiziellen Regeln vereinbart haben, vorhersehbar war.</li>
		<li>Der Sponsor haftet nicht für Schäden, die durch eine leicht fahrlässige Verletzung einer nicht wesentlichen vertraglichen Verpflichtung entstehen.</li>
		<li>Soweit wegen Fahrlässigkeit Schadenersatzansprüche gegen den Sponsor, dessen Mitarbeiter, Vertreter oder Helfer bestehen sollten, verjähren solche Ansprüche ein Jahr nach ihrer Entstehung, vorausgesetzt, dass der Benutzer von dem betreffenden Anspruch wusste.</li>
		<li>Soweit die Haftung des Sponsors beschränkt oder ausgeschlossen ist, gilt dasselbe in Bezug auf jegliche persönliche Haftung der rechtlichen Vertreter, Mitarbeiter oder Helfer des Sponsors.</li>
	</ul>
	</li>

	<li><strong>STREITIGKEITEN:</strong>  Diese offiziellen Regeln unterliegen deutschem Recht. Gerichtsstand ist München, außer wenn der Teilnehmer als Verbraucher gilt.</li>

	<li><strong>OFFIZIELLE REGELN/GEWINNERLISTE:</strong> Um eine offizielle Gewinnerliste (verfügbar nach dem 31.07.2014) oder eine Ausfertigung dieser offiziellen Regeln (vor dem 01.07.2014) zu erhalten, senden Sie einen mit mit Rückadresse und Porto versehenen Briefumschlag an: Pitch Perfect 2 Caption Contest - WINNERS LIST <ODER> OFFICIAL RULES (bitte geben sie entweder das Eine oder das Andere an), c/o Brandmovers Inc., 1575 Northside Drive, Bldg. 200, Suite 200, Atlanta, GA 30318. Die offiziellen Regeln sind während des Wettbewerbszeitraums auch unter <a href="https://pitchperfect2.brandmovers.net/pp2germancontest" target="_blank">https://pitchperfect2.brandmovers.net/pp2germancontest</a> abrufbar.</li>
</ol>

	
	<h3>ERKLÄRUNG ZUR EIGNUNG UND PUBLIZITÄT</h3>

	<p><strong>Wettbewerb:</strong> "Pitch Perfect 2" - Bildbeschriftungswettbewerb</p>

	<p><strong>Sponsor:</strong>	Universal Pictures International Germany GMBH, Hahnstrasse 31-35, 60528 Frankfurt, Deutschland.</p>
	<p>BITTE BEACHTEN SIE, DASS DIESE ERKLÄRUNG INNERHALB VON 48 STUNDEN NACH ERHALT ZURÜCKGESANDT WERDEN MUSS</p>

	<p>Senden Sie die ausgefüllte Erklärung per E-Mail an:	<a href="mailto:klyons@brandmovers.com">klyons@brandmovers.com</a><br />
	Betreffzeile: Pitch Perfect 2 Caption Contest - Germany<br />
	ODER<br />
	faxen Sie die ausgefüllte Erklärung an:	+1 678.718.1851, z. Hdn.: Keri Lyons – Pitch Perfect 2 Caption Contest - Germany</p>


	<ol start="12">

		<li><strong>Ein (1) Gewinner des Hauptpreises:</strong> Ein (1) Hauptpreispaket: Eine Reise für den Gewinner und einen (1) Gast (den „Gast”) nach Baton Rouge, LA („Ort des Hauptgewinns”) mit einem festgesetzten Reisedatum/Reisezeitraum vom 8. Juli – 11. Juli 2014, um das Set von Pitch Perfect 2 zu besuchen (die „Reise” oder der „Preis”). Die Reise beinhaltet: (a) Hin- und Rückflug in der Touristenklasse für den Gewinner und den Gast von einem größeren Flughafen in der Nähe des Wohnsitzes des Gewinners zu einem Flughafen nahe dem Ort des Hauptgewinns und zurück (beide Flughäfen werden vom Sponsor bestimmt und Zwischenlandungen sind möglich); (b) zwei (2) aufeinanderfolgende Übernachtungen in einem Hotel nach Wahl des Sponsors (ein Standardzimmer mit Doppelbelegung, nur Zimmer und Steuern); (c) Zubringerfahrten zwischen dem Flughafen und dem Hotel sowie zum Set des Pitch Perfect 2; (d) $150 pro Nacht zur Verwendung für Speisen und Getränke im Hotel und (e) einen Besuch des Sets von Pitch Perfect 2.</li>
	</ol>

		<p>GESAMTWERT des Hauptpreises: 5.300,00 US-Dollar</p>

		<p>Die Reise muss vom 8. Juli 2014 – 11. Juli 2014 angetreten werden, sonst verfällt der Hauptpreis und der Sponsor hat dann keine weiteren Verpflichtungen gegenüber dem betreffenden Gewinner. </p> 

		<p>Der Gewinner und der Gast müssen nach derselben Reiseplanung reisen.</p>

		<p>Der Unterzeichnende erklärt hiermit, gewährleistet und verpflichtet sich wie folgt:</p>

		<ol>
			<li>Ich unterfertige und überreiche diese Erklärung in Kenntnisnahme dessen, dass man sich auf sie und auf mein Einverständnis mit den darin enthaltenen Bedingungen verlassen wird, um meine Eignung dafür festzustellen, dass ich den oben beschriebenen Preis erhalte, nachdem meine Einsendung im oben angeführten Wettbewerb ausgewählt worden ist. </li>
			<li>Ich habe die offiziellen Regeln des Wettbewerbs, auf die hiermit verwiesen wird und die hier eingeschlossen werden, gelesen, verstanden und eingehalten, und ich werde sie weiterhin einhalten.</li>

			<li>Ich werde dafür sorgen, dass mein Reisebegleiter/Gast, der mich bei der Reise, die den Hauptgewinn darstellt, begleiten wird, die hier beigefügte Freigabe des Reisebegleiters unterzeichnet und bis zum oben angeführten Abgabetermin an den Sponsor unter der nachfolgend angeführten Adresse auf eine der drei oben angeführten Weisen zurücksendet, bevor er das Recht erhält, an der zum Preis gehörenden Reise teilzunehmen. </li>

			<li>Ich gewähre hiermit dem Sponsor und seinen angeschlossenen Unternehmen im ganzen Universum und für alle Ewigkeit das bedingungslose Recht, meinen Vornamen, meine Adresse (nur Stadt und Bundesland) sowie, nach Einholung meiner gesonderten ausdrücklichen Zustimmung, meine Stimme, mein Aussehen, mein Foto, biografische und preisbezogene Informationen, meine Aussagen über die Werbeaktion und/oder Live-Übertragungen oder Aufzeichnungen von Interviews mit mir für beliebige Programmier-, Publizitäts-, Werbe- und Aktionszwecke im Zusammenhang mit dem Wettbewerb und dem Spielfilm „PitchPerfect2” für Waren bzw. Dienstleistungen des Sponsors und seiner verbundenen Unternehmen zu nutzen, ohne dass eine zusätzliche Vergütung anfällt. Ich nehme hiermit zur Kenntnis, dass der Sponsor bzw. von ihm beauftragte Personen vor, nach oder während der Reise Bilder oder andere Aufnahmen für die vorstehend angeführten Zwecke machen können, nachdem sie meine ausdrückliche Zustimmung erhalten haben, auf die sie sich berufen können.</li>
			<li>Ich gewähre dem Sponsor und seinen Vertretern das unbefristete, unbeschränkte, nicht ausschließliche, weltweite, übertragbare und unwiderrufliche Recht erteilt, den Beitrag und alle seine Elemente, einschließlich aller Namen und Ähnlichkeiten, auf beliebige Weise und in allen beliebigen, heute bekannten oder erst in Zukunft entwickelten Medien zu modifizieren, abgeleitete Werke davon zu schaffen, sie zu bearbeiten, zu veröffentlichen oder anderweitig zu nutzen, ohne dass eine weitere Entlohnung anfällt oder eine Benachrichtigung oder Genehmigung nötig ist.</li>
			<li>Ich verstehe, dass die Bedingungen dieser Erklärung und Freigabe vertraulich sind, und ich erkläre mich damit einverstanden, sie Dritten gegenüber nicht offenzulegen, außer, wenn dies aufgrund der Gesetze oder infolge einer Vorladung oder anderer Informationsersuchen eines Gerichts oder einer staatlichen Behörde erforderlich ist.</li>
		</ol>

		<p>Ich erkläre, dass alle Aussagen, die ich in diesem Dokument mache, wahr sind.  Der unten angeführte Name ist mein rechtsgültiger Name.</p>
		<p>WEITERE ANGABEN HABE ICH IN DIESEM ZUSAMMENHANG NICHT ZU MACHEN.</p>

		<pre>
Datum														Telefonnummer


Name des Gewinners (in Druckbuchstaben bitte)   							Geburtsdatum

__________________________________________________________	
	

Name des Erziehungsberechtigten (in Druckbuchstaben bitte), falls nötig			Geburtsdatum

__________________________________________________________  												  

Adresse des Gewinners
 						  	


Unterschrift des Gewinners</pre>

<h3>FREIGABE – ZUR UNTERZEICHNUNG DURCH DEN GAST</h3>

<h3>VOR UNTERZEICHNUNG BITTE AUFMERKSAM LESEN</h3>

<pre>
Name des Gastes: 											

Erziehungsberechtigter, falls der Gast minderjährig ist: 								    

Adresse: 												

Stadt, Bundesland, Postleitzahl:												

Telefonnummer: Tagsüber:  					    Abends: 				

E-Mail-Adresse:  											

Geburtsdatum:
</pre>

<p>Ich reiche diese Reisebegleiterfreigabe an die Universal Pictures International Germany GMBH („Sponsor”) ein, da ein Gewinner des Pitch Perfect 2 Bildunterschriftwettbewerbs („Wettbewerb”) mich als Reisebegleiter und Gast für den Preis benannt hat, der in der Erklärung und Freigabe des Preisgewinners beschrieben ist. Im Gegenzug für den Genuss des Preises nehme ich Folgendes zur Kenntnis und erkläre mein Einverständnis hierzu:</p>

<ol>
	<li>Ich (oder ich im Namen meines minderjährigen Kindes/Mündels, falls zutreffend) werde am Preis teilhaben.  Ich erkläre mich einverstanden, entsprechend der selben Reiseplanung zu reisen wie der Gewinner des Preises, und nehme zur Kenntnis, dass, wenn ich minderjährig bin, der Gewinner des Preises mein Erziehungsberechtigter sein muss.  Ich erkläre mich damit einverstanden, dass der Preis auf einer „Wenn in Anspruch genommen"-Grundlage angeboten wird und dass der Preis den Einschränkungen unterliegt, die in den offiziellen Regeln angeführt sind. </li>

	<li>Ich (oder ich im Namen meines minderjährigen Kindes/Mündels, falls zutreffend) gewähre hiermit dem Sponsor und seinen angeschlossenen Unternehmen im ganzen Universum und für alle Ewigkeit das bedingungslose Recht, meinen Vornamen, meine Adresse (nur Stadt und Bundesland) sowie, nach Einholung meiner gesonderten ausdrücklichen Zustimmung, meine Stimme, mein Aussehen, mein Foto, biografische und preisbezogene Informationen, meine Aussagen über die Werbeaktion und/oder Live-Übertragungen oder Aufzeichnungen von Interviews mit mir für beliebige Programmier-, Publizitäts-, Werbe- und Aktionszwecke im Zusammenhang mit dem Wettbewerb und dem Spielfilm „PitchPerfect2” für Waren bzw. Dienstleistungen des Sponsors und seiner verbundenen Unternehmen zu nutzen, ohne dass eine zusätzliche Vergütung anfällt. Ich nehme hiermit zur Kenntnis, dass der Sponsor bzw. von ihm beauftragte Personen vor, nach oder während der Reise Bilder oder andere Aufnahmen für die vorstehend angeführten Zwecke machen können, nachdem sie meine ausdrückliche Zustimmung erhalten haben, auf die sie sich berufen können.</li>
</ol> 

<p>Ich (oder ich im Namen meines minderjährigen Kindes/Mündels, falls zutreffend) bestätige, dass ich geschäftsfähig bin, um diese Freigabe des Reisebegleiters zu unterzeichnen, und wenn ich in Land meines Wohnsitzes als minderjährig gelte, dass mein gesetzmäßiger Erziehungsberechtigter diese in meinem Namen unterzeichnet.</p>

<p>Name des Gastes:</p>						

<p>Unterschrift des Gastes (oder seines Erziehungsberechtigten, falls der Gast minderjährig ist):</p> 

<p style="margin:50px 0 0 50px;">Datum der Unterschrift:</p>

</div>
</body>
</html>

<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_Controller extends MX_Controller 
{
	public $user;
	protected $require_login = TRUE;

	function __construct()  
	{
       	parent::__construct();
		$this->output->enable_profiler(FALSE);
		
		if ( ! isset($_SESSION) && ! $this->input->is_cli_request())
		{
			session_start();
		}
		
		$this->load->database();
		$this->load->config('dashboard/dashboard');
		$this->load->helper(array('url', 'dashboard/config'));		
		$this->load->library(array('form_validation', 'template'));
		$this->form_validation->CI =& $this; // must come before the DashboardUser entity
		$this->load->entity('dashboard/DashboardUser');

		$this->template->add_theme_location(APPPATH . 'modules/');
		$this->template->set_theme('dashboard');
		
		if (isHttps() && strpos(base_url(), 'https://') === FALSE)
		{
			redirect(str_replace('http://', 'https://', current_url()));
		}

		$this->user = new DashboardUser();
		$this->template->set('user', $this->user);

		// Make sure the user is Logged in. Otherwise, redirect to Login.
		if ($this->require_login && ! $this->user->logged_in() && strtolower(substr($_SERVER['REQUEST_URI'], 0, 16)) != '/dashboard/login')
		{
			redirect('/dashboard/login');
		}
    }
}


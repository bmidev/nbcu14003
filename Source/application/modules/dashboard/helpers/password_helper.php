<?php

function encryptPassword($password) 
{
	$CI =& get_instance();
	$salt = $CI->config->item('password_salt');
	return sha1($salt.$password.$salt);
}

function generatePassword($num_characters=10) 
{
    $character_set = array(
      'c', 'C', 'd', 'D', 'f', 'F', 'g', 'G', 'h',
      'H', 'j', 'J', 'k', 'K', 'm', 'M', 'n', 'N',
      'p', 'P', 'q', 'Q', 'r', 'R', 't', 'T', 'v',
      'V', 'w', 'W', 'x', 'X', 'y', 'Y', 'z', 'Z',
      '2', '3', '4', '6', '7', '9');
      
   	$bad_sets = array(
      'btc', 'btt', 'chn', 'cck', 'dmn', 'dyk', 'fgg',
      'fck', 'hrd', 'hnk', 'kyk', 'ngg', 'pck', 'prc',
      'whr');
  
    $password = null;
    $character_set_size = count($character_set);
    while (!$password) 
    {
		for ($i = 0; $i < $num_characters; $i++) 
		{
			$password .= $character_set[rand(0, ($character_set_size - 1))];
		}
      
		$match_found = false;
		foreach ($bad_sets as $bad_set) 
		{
			if (stripos($password, $bad_set) !== false) 
			{
				$match_found = true;
			}
		}
		
		if ($match_found) { $password = false; }
    }
    return $password;  
}
<?php

// ==============================================================================
//  These are shortcut functions to get values from CodeIgniter's config files.   
// ==============================================================================

function now() 
{
    $CI =& get_instance();
	return $CI->config->item('current_date');
}

function appName() 
{
    $CI =& get_instance();
	return $CI->config->item('app_name');
}

function appTitle() 
{
    $CI =& get_instance();
    if ($CI->config->item('app_title') && $CI->config->item('app_title') != '')
    {
    	return $CI->config->item('app_title');
	}
	return FALSE;
}

function appLogo() 
{
    $CI =& get_instance();
	return $CI->config->item('app_logo');
}

function appLogoPadding() 
{
    $CI =& get_instance();
	return $CI->config->item('app_logo_padding');
}

function appIcon() 
{
    $CI =& get_instance();
	return $CI->config->item('app_icon');
}

function appStartDate($format = 'U') 
{
    $CI =& get_instance();
	return date($format, $CI->config->item('app_start_date'));
}

function appEndDate($format = 'U') 
{
    $CI =& get_instance();
	return date($format, $CI->config->item('app_end_date'));
}

function gaEmail() 
{
    $CI =& get_instance();
	return $CI->config->item('ga_email');	
}

function gaPassword() 
{
    $CI =& get_instance();
	return $CI->config->item('ga_password');	
}

function gaProfileId() 
{
    $CI =& get_instance();
	return $CI->config->item('ga_profile_id');	
}

function cachingOn() 
{
    $CI =& get_instance();
	return $CI->config->item('caching');
}

function cachingTime()
{    
	$CI =& get_instance();
	return $CI->config->item('caching_time');
}

function isHttps()
{    
	$CI =& get_instance();
	return $CI->config->item('https');
}
<?php 
	
	// ===============================
	//  Is the request method a post?   
	// ===============================
	
	function is_post() 
	{
		$CI =& get_instance();
		return (strtolower($CI->input->server('REQUEST_METHOD')) == 'post') ? true : false;
	}


	// ======================
	//  Generate page links.   
	// ======================
	
	function generatePageLinks($page, $total_pages, $prefix, $padding) {
		$str = '';
		
		// Prev Arrow
		if ($page != 1) { $str .= '<li class="first"><a href="' . $prefix . '1">First</a></li>' . "\n\t\t"; }
		if ($page != 1) { $str .= '<li class="prev"><a href="' . $prefix . ($page-1) . '">&#60;&#60;</a></li>' . "\n\t\t"; }
		
		// Left Padding
		for ($i = $padding; $i >= 1; $i--) {
			if ($page > $i) { $str .= '<li><a href="' . $prefix . ($page - $i) . '">' . ($page - $i) . '</a></li>' . "\n\t\t"; }
		}		
		// Current Page
		$str .= '<li>' . $page . '</li>' . "\n\t\t";
		
		// Right Padding
		for ($i = 1; $i <= $padding; $i++) {
			if (($page + $i) <= $total_pages) { $str .= '<li><a href="' . $prefix . ($page + $i) . '">' . ($page + $i) . '</a></li>' . "\n\t\t"; }
		}	
		
		// Next Arrow
		if ($page != $total_pages) { $str .= '<li class="next"><a href="' . $prefix . ($page+1) . '">&#62;&#62;</a></li>' . "\n\t\t"; }	
		if ($page != $total_pages) { $str .= '<li class="last"><a href="' . $prefix . $total_pages . '">Last</a></li>' . "\n\t\t"; }
		return $str;
	}


	// ============================================
	//  Convert seconds to Hours, Minutes, Seconds   
	// ============================================
	
	function sec2hms ($sec, $pad_hours = false) 
	{ 
	    $hms = '';
	    $hours = intval(intval($sec) / 3600); 
		$hms .= ($pad_hours) 
	          ? str_pad($hours, 2, "00", STR_PAD_LEFT). ':'
	          : $hours. ':';
	    $minutes = intval(($sec / 60) % 60); 
	    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). ':';
	    $seconds = intval($sec % 60); 
	    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);
	    return $hms;
	}	
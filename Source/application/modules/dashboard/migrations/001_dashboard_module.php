<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Dashboard_module extends CI_Migration
{
	public function _construct()
	{
		// Load the database.
		$this->load->database();
	}
	
	public function up()
	{
		$sql = <<<SQL
CREATE TABLE `dashboard_users` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `status` ENUM('Active','Inactive') NOT NULL DEFAULT 'Active' ,
  `email` VARCHAR(100) NOT NULL DEFAULT '' ,
  `name` VARCHAR(100) NOT NULL DEFAULT '' ,
  `password` VARCHAR(40) NOT NULL DEFAULT '' ,
  `created_at` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `password` (`password` ASC) ,
  INDEX `email` (`email` ASC)
) ENGINE = InnoDB;
INSERT INTO `dashboard_users` (`email`,`name`,`password`,`created_at`) VALUES ('amitchell@brandmovers.com', 'Andrew Mitchell', 'b59ead9e0f88efdb706c13a1d9a02c8c0018c9b0', NOW());
SQL;
		foreach (explode(';', $sql) as $query)
		{
			if (empty($query)) continue;
			$this->db->query(trim($query));
		}
	}
	
	public function down()
	{
		$sql = 'DROP TABLE IF EXISTS `dashboard_users`';
		$this->db->query($sql);
	}
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(dirname(__FILE__)).'/libraries/Controllers.php';

class Dashboard extends Dashboard_Controller 
{	 
	function __construct()
	{
		parent::__construct();

		$this->load->helper('form');

		// Load Entities as needed.
		$this->load->entity(array(
			'dashboard/Stats',
			'dashboard/charts/LineChart',
			'dashboard/charts/PieChart',
			'dashboard/charts/StateMap',
			'dashboard/charts/WorldMap'
		));

		// Load Data Accessor Models.
		$this->load->model(array(
			'dashboard/ga_model',
			'dashboard/promotions_model',
		));

		// Configure the Template library
		$this->template->set_layout('bootstrap');
		$this->template->set_partial('nav', 'partials/nav');
		$this->template->inject_partial('page_id', 'dashboard');
		$this->template->title(appName(), appTitle());
	}

	function index()
	{
		// Start Date / End Date
		$start_date = appStartDate();
		$end_date = appEndDate();
				
		// Options & Filters in URI
		if ($this->input->get('start_date'))
		{
			$start_date = strtotime($this->input->get('start_date'));
		}
		if ($this->input->get('end_date'))
		{
			$end_date = strtotime($this->input->get('end_date'));
		}
		
		if ($start_date < appStartDate())
		{
			$start_date = appStartDate();
		}
		
		if ( ! $end_date || $end_date > time() || $end_date < $start_date)
		{ 
			$end_date = time() - (date('s', time()) + (date('i', time()) * 60)) - 3600;
		}
		
		// Line Chart of Daily Visitors
		$options = array('chartId' => 'visitors', 'width' => '938');
		$data['visitors'] = new LineChart($this->ga_model->dailyVisits(gaProfileId(), $start_date, $end_date), $options, $start_date, $end_date); 
		
		// Basic Stats from Google Analytics
		$data['basic'] = new Stats($this->ga_model->basicStats(gaProfileId(), $start_date, $end_date));		

		$colors = array('#5B5C77', '#D9D676','#90D669', '#8EAC4F', '#C98956', '#1C82D8');

		// Pie Charts
		$options_mobile = array('width' => 280, 'height' => 180, 'chart_id' => 'mobile_pie_chart', 'colors' => $colors);		
		$options_browsers = array('width' => 280, 'height' => 180, 'chartId' => 'browser_pie_chart', 'colors' => $colors);
		$options_mediums = array('width' => 280, 'height' => 180, 'chartId' => 'medium_pie_chart', 'colors' => $colors);		
		$data['mobile'] = new PieChart($this->ga_model->mobileVisitors(gaProfileId(), $start_date, $end_date, 5), $options_mobile);		
		$data['browsers'] = new PieChart($this->ga_model->getVisitorsByBrowsers(gaProfileId(), $start_date, $end_date, 5), $options_browsers);
		$data['mediums'] = new PieChart($this->ga_model->getVisitorsByMedium(gaProfileId(), $start_date, $end_date, 5), $options_mediums);
		
		// United States Map
		$data['states'] = new StateMap($this->ga_model->getVisitorsFromUnitedStates(gaProfileId(), $start_date, $end_date));
		$data['countries'] = new WorldMap($this->ga_model->getVisitorsByCountry(gaProfileId(), $start_date, $end_date));
		
		// Promotion metrics
		//$data['registrations'] = $this->promotions_model->totalRegistrations($start_date, $end_date);
		//$data['optins'] = $this->promotions_model->totalOptins($start_date, $end_date);
		//$data['entries'] = $this->promotions_model->totalEntries($start_date, $end_date);
		//$data['entries_registration'] = $this->promotions_model->totalEntriesByReason($start_date, $end_date, 'Registration');
		//$data['shares'] = $this->promotions_model->totalShares($start_date, $end_date);
		//$data['shares_facebook'] = $this->promotions_model->totalSharesByNetwork($start_date, $end_date, 'facebook');
		//$data['shares_twitter'] = $this->promotions_model->totalSharesByNetwork($start_date, $end_date, 'twitter');

		// Set up the view
		$data['view'] = 'dashboard';
		$data['javascript'] = array('jquery','google_jsapi','jquery-ui');
		$data['form'] = array(
			'start_date' => date('Y-m-d', $start_date),
			'end_date' => date('Y-m-d', $end_date)
		);		
		
		$this->template->build('pages/index', $data);
	}
}

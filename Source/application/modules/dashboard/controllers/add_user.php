<?php

class Add_user extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		if ( ! $this->input->is_cli_request())
		{
			show_404();
		}
		
		$this->load->database();
		$this->load->config('dashboard');
		$this->load->helper(array('password', 'url'));
	}
	
	public function _remap()
	{
		$this->index();
	}
	
	public function index()
	{
		// Preconfigure options.
		$options = array();
		
		// Fetch the user input.
		$input_argc = $this->input->server('argc');
		$input_argv = $this->input->server('argv');
		
		// Remove script name, controller and method to isolate the arguments.
		$method_position = 0;
		foreach ($input_argv as $key => $value)
		{
			if (strtolower($value) == 'add_user')
			{
				$method_position = $key;
			}
		}
		$options = array_splice($input_argv, ($method_position + 1));
		
		// Display script help banner
		if (in_array('--help', $options) || empty($options))
		{
			echo <<<TXT

Adds a user to the dashboard panel by email address. Passwords are automatically
assigned.

COMMAND:
 $ php htdocs/index.php dashboard add_user user@place.com:"User Name"

EXAMPLE:
 $ php htdocs/index.php dashboard add_user jhill@brandmovers.com:"Jonathon Hill"
 
 The user name portion is optional. This will work just as well:
 
 $ php htdocs/index.php dashboard add_user jhill@brandmovers.com


TXT;
			exit(0);
		}
		
		// Extract the email and user name
		@list($email, $user_name) = explode(':', $options[0]);
		$email = trim($email);
		$user_name = trim($user_name, '" ');
		if (empty($user_name)) $user_name = $email;
		
		// Create a random password
		$password = generatePassword();
		
		// Add the user
		$sql = $this->db->insert_string('dashboard_users', array(
			'email' => $email,
			'name'  => $user_name,
			'password' => encryptPassword($password),
			'created_at' => date('Y-m-d H:i:s'),
		));
		$this->db->query($sql);
		
		$url = site_url('/dashboard/login');
		
		echo <<<TXT

User added!

$user_name
----------
Login URL: $url
Username:  $email
Password:  $password


TXT;

		exit(0);
	}
}


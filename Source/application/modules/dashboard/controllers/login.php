<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(dirname(__FILE__)).'/libraries/Controllers.php';

class Login extends Dashboard_Controller 
{
	protected $require_login = FALSE;

	function __construct()
	{
		parent::__construct();
		$this->load->helper('dashboard/password');
		$this->load->model('dashboard/dashboard_model');	
	}

	// ======================================================= 
	//  This method loads the login form and validates posts.   
	// ======================================================= 

	function index() 
	{
		// If user is already logged in, redirect to homepage.
		if ($this->user->logged_in())
		{
			redirect('/dashboard');
		}
		
		$this->load->helper(array('app', 'form'));
		if (is_post()) 
		{
			$this->user = new DashboardUser($this->dashboard_model->by_email($this->input->post('email')));
			if ($this->user->validate_login()) 
			{
				$this->user->login();
				redirect('/dashboard');
			}
		}

		$data['view'] = 'login';
		$data['javascript'] = array('jquery', 'dashboard');
		
		$this->template->set_layout('login');
		$this->template->build('pages/login', $data);
	}
	
	// ============================ 
	//  Log out the current user.   
	// ============================
	
	function logout() 
	{	
		$this->user->log_out();
		redirect('/dashboard/login'); 
	}

	// ============================= 
	//  Check if password is valid.   
	// ============================= 
	
    function valid_password($value, $params) 
    {  
		$this->form_validation->set_message('valid_password', 'Incorrect Email/Password Combination.');
		return (encryptPassword($value) == $params);
    }
}


DROP TABLE IF EXISTS `dashboard_users` ;

CREATE  TABLE IF NOT EXISTS `dashboard_users` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `status` ENUM('Active','Inactive') NOT NULL DEFAULT 'Active' ,
  `email` VARCHAR(100) NOT NULL DEFAULT '' ,
  `name` VARCHAR(100) NOT NULL DEFAULT '' ,
  `password` VARCHAR(40) NOT NULL DEFAULT '' ,
  `created_at` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `password` (`password` ASC) ,
  INDEX `email` (`email` ASC) )
ENGINE = InnoDB;


/*----- Insert Andy's Credentials ------*/

INSERT INTO `dashboard_users` (`email`,`name`,`password`,`created_at`) VALUES ('amitchell@brandmovers.com', 'Andrew Mitchell', 'b59ead9e0f88efdb706c13a1d9a02c8c0018c9b0', NOW());

/*----- Insert Dashboard User ------*/

SET @password = 'G3nb349M';
SET @email = 'user@brandmovers.com';
SET @name = 'User';
SET @salt = '8qYNUY9JAh6B1wX2Vwl90fhDgCqff2qxMtyscUEmvHlzUy6leeUmk2NJdDag';

INSERT INTO `dashboard_users` (`email`,`name`,`password`,`created_at`) VALUES (@email, @name, SHA1(CONCAT(@salt, @password, @salt)), NOW());

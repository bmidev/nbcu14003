<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class is for validating login credentials.
 * 
 * @exception 
 * @author Brad Estey
 * @package BMI Component
 */
 
class DashboardUser 
{
	private $data = array();
	private $valid_fields;
	private $CI;

	public function __construct($data = FALSE) 
	{		
		if ($data) { $this->data = $data; }	
		$this->CI =& get_instance();
		$this->valid_fields = array('id','name','email');	
	}
	
	
	// ================================== 
	//  Validate user login credentials.   
	// ==================================

	public function validate_login() 
	{
		$password = null;
		
		if (isset($this->data['password']))
		{
			$password = $this->data['password'];
		} 
			
		$this->CI->form_validation->set_rules('email', 'Email', 'trim|valid_email|required|max_length[100]|strip_tags');
		$this->CI->form_validation->set_rules('password', 'Password', 'callback_valid_password[' . $password . ']|trim|required|max_length[20]|strip_tags');
		$this->CI->form_validation->set_message('valid_password', 'Incorrect Email/Password Combination.');

		if ( ! $this->CI->form_validation->run()) 
		{ 
			return FALSE;
		} 
		else 
		{
			return TRUE;
		}
	} 


	// ============== 
	//  Log in user.   
	// ==============

	public function login() 
	{
		$this->set_data($this->data);
	}


	// ============================= 
	//  Check is user is logged in.   
	// ============================= 

	public function logged_in() 
	{
		if (isset($_SESSION['bmvr_dashboard']['id'])) { return TRUE; } 
		return FALSE;		
	}


	// ================ 
	//  Log out user.   
	// ================ 
	
	public function log_out() 
	{
		unset($_SESSION['bmvr_dashboard']);
	}


	// ==================================== 
	//  Set $_SESSION['bmvr_dashboard'] array.   
	// ==================================== 
	
	public function set_data($data) 
	{
		if (!empty($data)) 
		{
			foreach($data as $key => $value) 
			{
				if (in_array($key, $this->valid_fields)) 
				{
					$_SESSION['bmvr_dashboard'][$key] = $value;
				}				
			}
			return TRUE;
		}
		return FALSE;		
	}
	
	
	// ============================================== 
	//  Get data from $_SESSION['bmvr_dashboard'] array.   
	// ============================================== 
	
	public function get_data() 
	{
		if (isset($_SESSION['bmvr_dashboard'])) 
		{ 
			return $_SESSION['bmvr_dashboard']; 
		}
		return FALSE;	
	}

	
	// ====================================================================
	//  Magic Method: Get and Set data into $_SESSION['bmvr_dashboard'] array.   
	// ====================================================================
	
	public function __call($name, $parameters) 
	{
		if (strpos($name, 'get_') === 0) 
		{
			if (!isset($_SESSION['bmvr_dashboard'])) { return FALSE; }
			$name = preg_replace('/^get_/','',$name);
			
			if (array_key_exists($name, $_SESSION['bmvr_dashboard'])) 
			{ 
				return $_SESSION['bmvr_dashboard'][$name]; 
			}				
		}

		if (strpos($name, 'set_') === 0) 
		{
			$name = preg_replace('/^set_/','',$name);

			if (in_array($name, $this->valid_fields)) 
			{ 
				$_SESSION['bmvr_dashboard'][$name] = $parameters[0]; 
				return TRUE;
			}		
		}		
		
		return FALSE;
	}
}

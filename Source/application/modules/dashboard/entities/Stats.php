<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class generates a basic list of stats.
 * 
 * @exception 
 * @author Brad Estey
 * @package BMI Component
 */
 
class Stats {

	private $data = array();
	
	public function __construct($data = FALSE, $humanize = FALSE) {
		// Set data.
		if ($data) { $this->data = $data; }	
	}
	
	// ============================== 
	// ! Accessors and Manipulators   
	// ============================== 	

	// Get Raw Data.	
	public function getData($humanize) {
	    if (is_array($this->data)) { return $this->data; }
	    else { return FALSE; }
	}		

	// Set Chart Data.	
	public function setData() {
		if ($data) { $this->data = $data; }
	}
	
	// Humanize numbers with commas and such.
	public function humanizedData($name = FALSE) {
		$data = array();
		foreach($this->data as $key => $value) { 
			if (is_numeric($value) && $value == intval($value)) {
				$data[$key] = number_format($value, 0, '.', ','); 
			}
		}

		if ($name) { 
			if (isset($data[$name])) { return $data[$name]; }
			if (isset($this->data[$name])) { return $this->data[$name]; }
			return FALSE;
		}
		$data = $data + $this->data;
		return $data;
	}

	// Get Chart Data by Key. 
	public function __call($name, $parameters = FALSE) {	

		$name = preg_replace('/^get/','',$name);
		// lcfirst() doesn't work on all our servers.. :(
		$name = strtolower(substr($name, 0, 1)) . substr($name, 1);
		
		if (array_key_exists($name, $this->data)) {
			if ($parameters) {  
				return $this->humanizedData($name); 
			} else {
				return $this->data[$name]; 
			}
		}
			
		
		
		return FALSE;
	}
	
}
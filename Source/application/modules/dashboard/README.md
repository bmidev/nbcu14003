ci/dashboard
============

Brandmovers Standard Reporting Dashboard

Dependencies
------------

* CodeIgniter with [Modular Extensions](https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc/wiki/Home)

Installation
------------

1. Add this to your project `composer.json`, and then run `composer install` or `composer update`:

   ```
   {
       "require": {
           "ci/dashboard": "*"
       }
   }
   ```
   
2. Copy the `assets` directory into your htdocs folder
3. Run the latest database migrations

To add a dashboard user, use the following script:

```
$ php htdocs/index.php dashboard add_user [email] [name]
```

Changelog
---------

* 0.3.0
  - Updated to a responsive Twitter Bootstrap front-end
  - Used HTML5 `date` and `email` fields, and fall back to jQuery UI calendar on IE8/9
  - Upgraded the line and pie charts to [Highcharts](http://www.highcharts.com)
  - Upgraded the map charts to [Google Geochart](https://developers.google.com/chart/interactive/docs/gallery/geochart)
  - Used the Template library and created layouts for the login page and dashboard page to facilitate re-use in other modules
  - Fixed a login bug where any password was accepted
  - Renamed the `User` model to `DashboardUser` to fix potential conflict with other modules
  - Renamed the `admin` database table to `dashboard_users`
  - Added this changelog
* 0.2.1
  - Fixed a datepicker bug
* 0.2.0
  - Converted to a module installable by Composer
* 0.1.5
  - Fixed bugs in Gapi.php and caching
  - Fixed an issue with the HTML markup
* 0.1.4
  - Original version by Brad Estey
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="content-language" content="en" />
	<meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title><?php echo $template['title']; ?></title>

	<link rel="stylesheet" type="text/css" media="all" href="/assets/dashboard/bootstrap/dist/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/dashboard/plugins/jquery-ui-1.10.3.custom/css/smoothness/jquery-ui-1.10.3.custom.min.css" />
	<style>
		html, body { height: 100%; }
		#wrap { min-height:100%; height:auto; margin: 0 auto -60px; padding: 0 0 60px; }
		#footer { height: 60px; }
		.caret.up { border-top: 0 dotted; border-bottom: 4px solid #000; }
	</style>
	
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="/assets/dashboard/js/html5shiv.js"></script>
      <script src="/assets/dashboard/js/respond.min.js"></script>
    <![endif]-->

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="/assets/dashboard/js/bootstrap.min.js"></script>
	<script src="/assets/dashboard/plugins/modernizr/js/modernizr.custom.56831.js"></script>
	
	<!-- Charts and visualizations -->
	<script type="text/javascript" src="//www.google.com/jsapi"></script>
	<script src="/assets/dashboard/js/highcharts/highcharts.js"></script>
	<script src="/assets/dashboard/js/highcharts/modules/exporting.js"></script>
	<script src="/assets/dashboard/plugins/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.js"></script>

	<!-- HTML5 date input fallback -->
	<script>
		$(function() {
			if (!Modernizr.inputtypes.date) {
				$('input[type="date"]').each(function() {
					var original_date = $(this).val();
					$(this).datepicker();
					$(this).datepicker("option", "dateFormat", 'yy-mm-dd');
					$(this).datepicker("setDate", original_date );
				});
			}
		});
	</script>
</head>
<body id="<?php echo $template['partials']['page_id']; ?>">

	<div id="wrap">
		<!-- Static navbar -->
		<div class="navbar navbar-inverse navbar-static-top">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a href="/dashboard" class="navbar-brand"><?php echo appName(); ?></a>
		    </div>
		    <div class="navbar-collapse collapse">
		      <?php echo $template['partials']['nav']; ?>
		      <ul class="nav navbar-nav navbar-right">
		        <li class="navbar-text hidden-xs"><em>Welcome,</em> <strong><?php echo $user->get_name(); ?></strong></li>
		        <li><a href="<?php echo site_url('/dashboard/logout'); ?>" id="logout">Log Out</a></li>
		      </ul>
		    </div><!--/.nav-collapse -->
		</div>

		<div class="clr"></div>

		<div style="padding: 0 15px;">
			<?php echo $template['body']; ?>
		</div>
	</div>

	<div id="footer" class="navbar-inverse navbar-static-bottom">
	    <div class="navbar-collapse collapse">
	      <p class="navbar-text"><small>Copyright &copy; <?php echo date('Y'); ?> Brandmovers, Inc. All Rights Reserved</small></p>
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="http://www.linkedin.com/company/brandmovers" target="_blank">LinkedIn</a></li>
	        <li><a href="http://twitter.com/brandmovers" target="_blank">Twitter</a></li>
	        <li><a href="http://www.facebook.com/Brandmovers" target="_blank">Facebook</a></li>
	        <li><a href="http://www.brandmovers.com" target="_blank">Brandmovers.com</a></li>
	      </ul>
	    </div><!--/.nav-collapse -->
	</div>

</body>
</html>
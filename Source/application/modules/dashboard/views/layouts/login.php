<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="content-language" content="en" />
	<meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title><?php echo $template['title']; ?></title>

	<link rel="stylesheet" type="text/css" media="all" href="/assets/dashboard/bootstrap/dist/css/bootstrap.min.css" />
	
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="/assets/dashboard/js/html5shiv.js"></script>
	<script type="text/javascript" src="//www.google.com/jsapi"></script>
      <script src="/assets/dashboard/js/respond.min.js"></script>
    <![endif]-->

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="/assets/dashboard/js/bootstrap.min.js"></script>
</head>
<body>

	<?php echo $template['body']; ?>

</body>
</html>
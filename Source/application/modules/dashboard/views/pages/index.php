<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Filters</h3>
            </div>
            <form class="panel-body form-inline" id="filter" action="/dashboard" method="get">
                <div class="form-group">
                    <label>Date Range</label>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="start_date">Start Date:</label>
                    <input type="date" class="form-control" id="start_date" name="start_date" value="<?php echo set_value('start_date', $form['start_date']); ?>" placeholder="Start Date" />
                </div>
                <div class="form-group">
                    <label class="sr-only" for="end_date">End Date:</label>
                    <input type="date" class="form-control" id="end_date" name="end_date" value="<?php echo set_value('end_date', $form['end_date']); ?>" placeholder="End Date" />
                </div>
                <button type="submit" class="btn btn-primary">Go</button>
            </form>
        </div>	
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Website Traffic</h3>
            </div>
            <div class="panel-body">
                <div class="chart">
                    <?php echo $visitors->generateChart(); ?>
                </div>
                <dl class="dl-horizontal col-md-4 col-sm-4">
                    <dt>Total Visits:</dt>
                    <dd><?php echo $basic->getVisits(TRUE); ?></dd>
                    <dt>Bounce Rate:</dt>
                    <dd><?php echo $basic->getBounceRate(); ?></dd>
                    <dt>Total Page Views:</dt>
                    <dd><?php echo $basic->getPageviews(TRUE); ?></dd>
                </dl>
                <dl class="dl-horizontal col-md-4 col-sm-4">
                    <dt>Avg. Time On Site:</dt>
                    <dd><?php echo $basic->getAvgTimeOnSite(); ?></dd>
                    <dt>Abs. Uniq. Visitors:</dt>
                    <dd><?php echo $basic->getVisitors(TRUE); ?></dd>
                    <dt>New Visits:</dt>
                    <dd><?php echo $basic->getNewVisits(TRUE); ?></dd>
                </dl>
                <dl class="dl-horizontal col-md-4 col-sm-4">
                    <dt>Pages/Visit:</dt>
                    <dd><?php echo $basic->getPagesPerVisit(); ?></dd>
                    <dt>% New Visits:</dt>
                    <dd><?php echo $basic->getNewVisitRate(TRUE); ?></dd>
                </dl>
            </div>
        </div>
    </div>
</div>

<!-- promotion-specific metrics
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Promotion Metrics</h3>
            </div>
            <div class="panel-body">
                <dl class="dl-horizontal col-md-4 col-sm-4">
                    <dt>Total Registrations:</dt>
                    <dd><?php //echo number_format($registrations); ?></dd>
                    <dt>Total Opt-ins:</dt>
                    <dd><?php //echo number_format($optins); ?></dd>
                </dl>
                <dl class="dl-horizontal col-md-4 col-sm-4">
                    <dt>Total Entries:</dt>
                    <dd><?php //echo number_format($entries); ?></dd>
                    <dt>Registration Entries:</dt>
                    <dd><?php //echo number_format($entries_registration); ?></dd>
                </dl>
                <dl class="dl-horizontal col-md-4 col-sm-4">
                    <dt>Total Shares:</dt>
                    <dd><?php //echo number_format($shares); ?></dd>
                    <dt>Facebook Shares:</dt>
                    <dd><?php //echo number_format($shares_facebook); ?></dd>
                    <dt>Twitter Shares:</dt>
                    <dd><?php //echo number_format($shares_twitter); ?></dd>
                </dl>
            </div>
        </div>
    </div>
</div>
-->

<div class="row">
    <div class="col-md-4 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Visits by Medium</h3>
            </div>
            <div class="panel-body">
                <?php echo $mediums->generateChart(); ?>
                <dl class="dl-horizontal">
                <?php foreach ($mediums->humanizedData() as $key => $value) : ?><dt><?php echo $key; ?>:</dt>
                    <dd><?php echo $value; ?></dd>
                <?php endforeach; ?>
                </dl>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Visits by Browser</h3>
            </div>
            <div class="panel-body">
                <?php echo $browsers->generateChart(); ?>
                <dl class="dl-horizontal">
                <?php foreach ($browsers->humanizedData() as $key => $value) : ?><dt><?php echo $key; ?>:</dt>
                    <dd><?php echo $value; ?></dd>
                <?php endforeach; ?>
                </dl>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Mobile Platform Visitors</h3>
            </div>
            <div class="panel-body">
                <?php echo $mobile->generateChart(); ?>
                <dl class="dl-horizontal">
                <?php foreach ($mobile->humanizedData() as $key => $value): ?>
                    <dt><?php echo $key; ?>:</dt>
                    <dd><?php echo $value; ?></dd>
                <?php endforeach; ?>
                </dl>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Visitors by Country</h3>
            </div>
            <div class="panel-body">
                <?php echo $countries->generateChart(); ?>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Visitors by State</h3>
            </div>
            <div class="panel-body">
                <?php echo $states->generateChart(); ?>
            </div>
        </div>
    </div>
</div>

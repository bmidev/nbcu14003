<div class="col-md-4" style="margin:auto; float:none;">
	
	<div id="logo" class="text-center">
		<img src="/assets/dashboard/<?php echo appLogo(); ?>" alt="<?php echo appName(); ?>" title="<?php echo appName(); ?>" style="padding: <?php echo appLogoPadding(); ?>;" />
	</div>

	<div class="page-header text-center">
	    <h1><?php echo appName(); ?></h1>
	</div>

	<form action="/dashboard/login" method="post" class="form-signin">

        <div class="form-group <?php if (form_error('email')) echo 'has-error'; ?>">
	        <label class="control-label" for="email">Email Address</label>
	        <input type="email" name="email" class="form-control input-lg" placeholder="Email address" value="<?php echo set_value('email'); ?>" autofocus>
			<?php echo form_error('email', '<p class="error help-block">', '</span>'); ?>
        </div>

        <div class="form-group <?php if (form_error('password')) echo 'has-error'; ?>">
	        <label class="control-label" for="password">Password</label>
	        <input type="password" name="password" class="form-control input-lg" placeholder="Password">
			<?php echo form_error('password', '<p class="error help-block">', '</span>'); ?>
        </div>

        <button type="submit" class="btn btn-primary btn-lg">Sign in</button>
    </form>
</div><!-- /.container -->

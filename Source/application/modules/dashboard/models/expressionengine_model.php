<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class manages data requests to the Coupons table.
 * 
 * @exception 
 * @author Brad Morse
 * @package BMI Component
 */
class Expressionengine_model extends CI_Model {
    /*
     * Get the number of General Inquiries
     */

    public function totalInquiries($start_date, $end_date, $inqType) {

        $data = 0;
        $start_date = date('Y-m-d', $start_date) . ' 00:00:00';
        $end_date = date('Y-m-d ', $end_date) . ' 23:59:59';

        /*
            retrieve form id from form_name (inqType) from table, to append to 
            table name in query below, freeform creates a new table for each form, to store its results,
            if the contact form has an id of 3, the form submissions will be stored in the exp_freeform_form_entries_3 table
        */
        $form_id_query = $this->db->select('form_id')->where('form_name', $inqType)->get('exp_freeform_forms');
        $form_id = $form_id_query->row('form_id');


        $this->db->select('COUNT(entry_id) as count');
        $this->db->where('entry_date >=', 'UNIX_TIMESTAMP("' .$start_date. '")', false);
        $this->db->where('entry_date <=', 'UNIX_TIMESTAMP("' .$end_date. '")', false);
        $query = $this->db->get('exp_freeform_form_entries_' . $form_id);

        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $data = $row['count'];
        }
        return $data;
    }

    /*
     * Get the number of entries from a channel
     */
    public function totalChannelEntries($start_date, $end_date, $short_name, $status = 'open') {
        $data = 0;
        $start_date = date('Y-m-d', $start_date) . ' 00:00:00';
        $end_date = date('Y-m-d ', $end_date) . ' 23:59:59';

        $sql = '
            SELECT 
                COUNT(exp_channel_titles.entry_id) as count
            FROM
                exp_channel_titles
            LEFT join 
                exp_channels 
            ON
                exp_channel_titles.channel_id = exp_channels.channel_id
            WHERE
                exp_channels.channel_name = "' . $short_name . '"
            AND
                exp_channel_titles.status = "' . $status . '"
            AND
                exp_channel_titles.entry_date >= UNIX_TIMESTAMP("' . $start_date . '")
            AND
                exp_channel_titles.entry_date <= UNIX_TIMESTAMP("' . $end_date . '");
        ';
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $data = $row['count'];
        }
        return $data;
    }
    

    public function freeformSubmissions($start_date, $end_date) {
        $form_id_query = $this->db->select('form_id')->where('form_name', $inqType)->get('exp_freeform_forms');
        $form_id = $form_id_query->row('form_id');

        $data = 0;
        $start_date = date('Y-m-d', $start_date) . ' 00:00:00';
        $end_date = date('Y-m-d ', $end_date) . ' 23:59:59';

        $this->db->select('form_field_3 as email, form_field_5 as message, form_field_11 as phone, form_field_13 as name, entry_date');
        
        $this->db->where('entry_date >=', 'UNIX_TIMESTAMP("' .$start_date. '")', false);
        $this->db->where('entry_date <=', 'UNIX_TIMESTAMP("' .$end_date. '")', false);
        $this->db->order_by('entry_date', 'desc');

        $query = $this->db->get('exp_freeform_form_entries_1');

        return $query->result_object();
    }
}

/* End of file expressionengine_model.php */
/* Location: ./application/models/data_model.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class manages dashboard table data requests.
 * 
 * @author Brad Estey
 * @package BMI Component
 */

class Dashboard_model extends CI_Model
{
	// ================================ 
	//  Get user data by email address   
	// ================================

	public function by_email($email) 
	{
		$sql = 'SELECT * FROM dashboard_users WHERE email = ? and status = "Active" LIMIT 1';
		$result = $this->db->query($sql, array($email));

		return ($result->num_rows() > 0) 
		     ? $result->row_array()
		     : FALSE;
	}
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class manages Google Analytics data requests.
 * 
 * @exception 
 * @author Brad Estey
 * @package BMI Component
 */
class Ga_model extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();		
        $this->load->driver('cache');
    }


	// =======================================
	//  Get Basic Stats. Cache Returned Data.   
	// =======================================

    public function basicStats($ga_profile_id, $start_date, $end_date) 
    {
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'basicStats-' . $start_date . '-' . $end_date;

		if (!$this->cache->file->get($cache_file) || !cachingOn()) 
		{ 
			$metrics = array('newVisits','visits','visitors','pageviews','uniquePageviews','avgTimeOnSite','bounces');
			
			$this->load->library('gapi', array('email' => gaEmail(), 'password' => gaPassword()));
        	$this->gapi->requestReportData($ga_profile_id, array(), $metrics, null, null, $start_date,  $end_date);
			$data = $this->gapi->getMetrics();
			
			// Create custom calculations.
			$this->load->helper('app');
			$custom['avgTimeOnSite'] = sec2hms(round($data['avgTimeOnSite']));
			if ($data['visits'] != 0) 
			{
				$custom['pagesPerVisit'] = round(($data['pageviews']/$data['visits']),2);
				$custom['bounceRate'] = round((($data['bounces']/$data['visits'])* 100),2) . '%';
				$custom['newVisitRate'] = round((($data['newVisits']/$data['visits'])*100),2) . '%';
			} 
			else 
			{
				$custom['pagesPerVisit'] = '0';
				$custom['bounceRate'] = '0%';
				$custom['newVisitRate'] = '0%';		
			}		
			$data = array_merge($data, $custom);
			
			if (cachingOn())
			{
				log_message('debug', 'Cache miss: '.$cache_file);
				$this->cache->file->save($cache_file, $data, cachingTime());
			}
		} 
		else 
		{
			log_message('debug', 'Cache hit: '.$cache_file);
			$data = $this->cache->file->get($cache_file);
		}
		return $data;
    }


	// ================================================ 
	//  Get Daily Total Visitors. Cache Returned Data.   
	// ================================================ 

	public function dailyVisits($ga_profile_id, $start_date, $end_date) 
	{
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'dailyVisits-' . $start_date . '-' . $end_date;

/*		$data['start_year'] = date('Y', $start_date); 
		$data['start_month'] = date('n', $start_date);
		$data['start_day'] = date('j', $start_date);
*/
		if (!$this->cache->file->get($cache_file) || !cachingOn()) 
		{ 
			$dimensions = array('year', 'month', 'day');
			$metrics = array('visits');
			$sort = array('year', 'month', 'day');
			
			$this->load->library('gapi', array('email' => gaEmail(), 'password' => gaPassword()));
        	$this->gapi->requestReportData($ga_profile_id, $dimensions, $metrics, $sort, null, $start_date, $end_date, 1, 300);
			
			foreach($this->gapi->getResults() as $result) 
			{
				$data[] = array
							(
								'label' => date('M j', strtotime($result->getYear() . '-' . $result->getMonth() . '-' . $result->getDay())),
								'point' => $result->getVisits(),
							);
			}
			
			if (cachingOn())
			{
				log_message('debug', 'Cache miss: '.$cache_file);
				$this->cache->file->save($cache_file, $data, cachingTime());
			}
		} 
		else 
		{
			log_message('debug', 'Cache hit: '.$cache_file);
			$data = $this->cache->file->get($cache_file);
		}
		return $data;
	}
	
	
	// ================================================= 
	//  Get Total Mobile Visitors. Cache Returned Data.   
	// =================================================
	
    public function mobileVisitors($ga_profile_id, $start_date, $end_date, $limit = FALSE) 
    {
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'mobileVisitors-' . $start_date . '-' . $end_date;
		
		if (!$this->cache->file->get($cache_file) || !cachingOn()) 
		{
			
			$metrics = array('visits');
			$dimensions = array('isMobile','operatingSystem');
			$filter = 'isMobile == Yes';
			$sort = array('-visits');		
		
			$this->load->library('gapi', array('email' => gaEmail(), 'password' => gaPassword()));
        	$this->gapi->requestReportData($ga_profile_id, $dimensions, $metrics, $sort, $filter, $start_date, $end_date);

			if ($limit) { $i = 1; }
			foreach($this->gapi->getResults() as $result) 
			{
				if ($limit) 
				{ 
					if ($i <= $limit) 
					{ 
						$data[$result->getOperatingSystem()] = $result->getVisits();
						$i++; 
					} 
					else 
					{
						if (!isset($data['Other'])) { $data['Other'] = 0; }
						$data['Other'] = $data['Other'] + $result->getVisits();
					}
				} 
				else 
				{
					$data[$result->getOperatingSystem()] = $result->getVisits();				
				}
			}

			if (cachingOn())
			{
				log_message('debug', 'Cache miss: '.$cache_file);
				$this->cache->file->save($cache_file, $data, cachingTime());
			}
		} 
		else 
		{
			log_message('debug', 'Cache hit: '.$cache_file);
			$data = $this->cache->file->get($cache_file);
		}
		
		return $data;
    }


	// ===================================================== 
	//  Get Total Visitors by Browser. Cache Returned Data.   
	// =====================================================
	 
    public function getVisitorsByBrowsers($ga_profile_id, $start_date, $end_date, $limit = FALSE) 
    {
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'getVisitorsByBrowsers-' . $start_date . '-' . $end_date;
		
		if (!$this->cache->file->get($cache_file) || !cachingOn()) 
		{
			$metrics = array('visits');
			$dimensions = array('browser');
			$sort = array('-visits');		
		
			$this->load->library('gapi', array('email' => gaEmail(), 'password' => gaPassword()));
        	$this->gapi->requestReportData($ga_profile_id, $dimensions, $metrics, $sort, null, $start_date, $end_date);
			
			if ($limit) { $i = 1; }
			foreach($this->gapi->getResults() as $result) 
			{
				if ($limit) 
				{ 
					if ($i <= $limit) 
					{ 
						$browser = trim($result->getBrowser());
						if ($browser == 'Mozilla Compatible Agent') { $browser = 'Mozilla'; }
						$data[$browser] = $result->getVisits();
						$i++; 
					} 
					else 
					{
						if (!isset($data['Other'])) { $data['Other'] = 0; }
						$data['Other'] = $data['Other'] + $result->getVisits();
					}
				} 
				else 
				{
					$data[$result->getBrowser()] = $result->getVisits();				
				}
			}
			
			if (cachingOn())
			{
				log_message('debug', 'Cache miss: '.$cache_file);
				$this->cache->file->save($cache_file, $data, cachingTime());
			}
		} 
		else 
		{
			log_message('debug', 'Cache hit: '.$cache_file);
			$data = $this->cache->file->get($cache_file);
		}
		
		return $data;
    }
 
 
	// ==================================================== 
	//  Get Total Visitors by Medium. Cache Returned Data.   
	// ==================================================== 

    public function getVisitorsByMedium($ga_profile_id, $start_date, $end_date, $limit = FALSE) 
    {
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'getVisitorsByMedium-' . $start_date . '-' . $end_date;
		
		if (!$this->cache->file->get($cache_file) || !cachingOn()) 
		{
			$metrics = array('visits');
			$dimensions = array('medium');
			$sort = array('-visits');		
		
			$this->load->library('gapi', array('email' => gaEmail(), 'password' => gaPassword()));
        	$this->gapi->requestReportData($ga_profile_id, $dimensions, $metrics, $sort, null, $start_date, $end_date);
			
			if ($limit) { $i = 1; }
			foreach($this->gapi->getResults() as $result) 
			{
				if ($result->getMedium() == '(none)') { $key = 'Direct'; } else { $key = ucfirst($result->getMedium()); }
				if ($limit) 
				{ 
					if ($i <= $limit) 
					{ 
						$data[$key] = $result->getVisits();
						$i++; 
					} 
					else 
					{
						if (!isset($data['Other'])) { $data['Other'] = 0; }
						$data['Other'] = $data['Other'] + $result->getVisits();
					}
				} 
				else 
				{
					$data[$key] = $result->getVisits();				
				}
			}
			
			if (cachingOn())
			{
				log_message('debug', 'Cache miss: '.$cache_file);
				$this->cache->file->save($cache_file, $data, cachingTime());
			}
		} 
		else 
		{
			log_message('debug', 'Cache hit: '.$cache_file);
			$data = $this->cache->file->get($cache_file);
		}
		
		return $data;
    } 


	// ====================================================================== 
	//  Get Total Visitors from United States by State. Cache Returned Data.   
	// ======================================================================
	
    public function getVisitorsFromUnitedStates($ga_profile_id, $start_date, $end_date, $limit = 50) {
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'getVisitorsFromUnitedStates-' . $start_date . '-' . $end_date;
		
		if (!$this->cache->file->get($cache_file) || !cachingOn()) 
		{
			$metrics = array('visits');
			$dimensions = array('region');
			$sort = array('-visits');		
			$filter = 'country == United States';

			$this->load->library('gapi', array('email' => gaEmail(), 'password' => gaPassword()));
        	$this->gapi->requestReportData($ga_profile_id, $dimensions, $metrics, $sort, $filter, $start_date, $end_date, 1, $limit);

			foreach($this->gapi->getResults() as $result) 
			{
				$data[] = array('state' => $result->getRegion(),
								'visits' => $result->getVisits());
			}
			
			if (cachingOn())
			{
				log_message('debug', 'Cache miss: '.$cache_file);
				$this->cache->file->save($cache_file, $data, cachingTime());
			}
		} 
		else 
		{
			log_message('debug', 'Cache hit: '.$cache_file);
			$data = $this->cache->file->get($cache_file);
		}
		
		return $data;
    } 


	// ====================================================================== 
	//  Get Total Visitors from United States by State. Cache Returned Data.   
	// ====================================================================== 
	
    public function getVisitorsByCountry($ga_profile_id, $start_date, $end_date, $limit = 50) 
    {
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'getVisitorsByCountry-' . $start_date . '-' . $end_date;
		
		if (!$this->cache->file->get($cache_file) || !cachingOn()) 
		{
			
			$metrics = array('visits');
			$dimensions = array('country');
			$sort = array('-visits');		

			$this->load->library('gapi', array('email' => gaEmail(), 'password' => gaPassword()));
        	$this->gapi->requestReportData($ga_profile_id, $dimensions, $metrics, $sort, null, $start_date, $end_date, 1, $limit);

			foreach($this->gapi->getResults() as $result) 
			{
				$data[] = array('country' => $result->getCountry(),
								'visits' => $result->getVisits());
			}
			
			if (cachingOn())
			{
				log_message('debug', 'Cache miss: '.$cache_file);
				$this->cache->file->save($cache_file, $data, cachingTime());
			}
		} 
		else 
		{
			log_message('debug', 'Cache hit: '.$cache_file);
			$data = $this->cache->file->get($cache_file);
		}
		
		return $data;
    }
    
    
	// ========================================== 
	//  Get Tracked Events. Cache Returned Data.   
	// ==========================================
    
    public function getEvents($ga_profile_id, $start_date, $end_date)
    {
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);	
		$cache_file = 'getEvents-' . $start_date . '-' . $end_date;
		
		if (!$this->cache->file->get($cache_file) || !cachingOn()) { 
			$dimensions = array('eventCategory', 'eventAction', 'eventLabel');
			$metrics = array('totalEvents');
			$sort = array('-totalEvents','eventCategory', 'eventAction', 'eventLabel');
 
			$this->load->library('gapi', array('email' => gaEmail(), 'password' => gaPassword()));
        	$this->gapi->requestReportData($ga_profile_id, $dimensions, $metrics, $sort, null, $start_date, $end_date, 1, 300);
			foreach($this->gapi->getResults() as $result) 
			{
				$data[$result->getEventCategory()][$result->getEventAction()][$result->getEventLabel()] = $result->getTotalEvents();
			}
			
			if (cachingOn())
			{
				log_message('debug', 'Cache miss: '.$cache_file);
				$this->cache->file->save($cache_file, $data, cachingTime());
			}
		} 
		else 
		{
			log_message('debug', 'Cache hit: '.$cache_file);
			$data = $this->cache->file->get($cache_file);
		}
				
		return $data;
    }    
}

/* End of file ga_model.php */
/* Location: ./application/models/ga_model.php */
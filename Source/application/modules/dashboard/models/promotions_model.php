<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class manages database requests to the registration table.
 * 
 * @exception 
 * @author Brad Estey
 * @package BMI Component
 */
class Promotions_model extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();		
        $this->load->driver('cache');
    }

    public function totalRegistrations($start_date, $end_date) 
    {
		$sql = 'SELECT COUNT(id) as `count` FROM users WHERE created_at BETWEEN ? AND ?';
		return $this->run($sql, 'totalRegistrations', $start_date, $end_date);
    }

    public function totalOptins($start_date, $end_date) 
    {
		$sql = 'SELECT COUNT(id) as `count` FROM users WHERE optin = "Yes" AND created_at BETWEEN ? AND ?';
		return $this->run($sql, 'totalOptins', $start_date, $end_date);
    }

    public function totalEntries($start_date, $end_date) 
    {
		$sql = 'SELECT COUNT(id) as `count` FROM sweepstakes_entries WHERE created_at BETWEEN ? AND ?';
		return $this->run($sql, 'totalEntries', $start_date, $end_date);
    }

    public function totalEntriesByReason($start_date, $end_date, $reason) 
    {
    	$cachePrefix = 'totalEntriesByReason-' . $reason;
    	$a = $this->db->escape($reason);
		$sql = 'SELECT COUNT(id) as `count` FROM sweepstakes_entries WHERE reason = ' . $a . ' AND created_at BETWEEN ? AND ?';
		return $this->run($sql, $cachePrefix, $start_date, $end_date);
    }

    public function totalPhotos($start_date, $end_date) 
    {
		$sql = 'SELECT COUNT(id) as `count` FROM photos WHERE created_at BETWEEN ? AND ?';
		return $this->run($sql, 'totalPhotos', $start_date, $end_date);
    }

    public function totalPhotosByRegistration($start_date, $end_date, $registered) 
    {
    	$cachePrefix = 'totalPhotosByRegistration-' . $registered;
    	$sql = $registered
    	     ? 'SELECT COUNT(id) as `count` FROM photos WHERE user_id IS NOT NULL AND created_at BETWEEN ? AND ?'
    	     : 'SELECT COUNT(id) as `count` FROM photos WHERE user_id IS NULL AND created_at BETWEEN ? AND ?';
		return $this->run($sql, $cachePrefix, $start_date, $end_date);
    }

    public function totalPhotosByFrame($start_date, $end_date, $frame) 
    {
    	$cachePrefix = 'totalPhotosByFrame-' . md5($frame);
    	$a = $this->db->escape($frame);
    	$sql = 'SELECT COUNT(id) as `count` FROM photos WHERE frame_url = ' . $a . ' AND created_at BETWEEN ? AND ?';
		return $this->run($sql, $cachePrefix, $start_date, $end_date);
    }

    public function totalShares($start_date, $end_date) 
    {
		$sql = 'SELECT COUNT(id) as `count` FROM shares WHERE created_at BETWEEN ? AND ?';
		return $this->run($sql, 'totalShares', $start_date, $end_date);
    }

    public function totalSharesByNetwork($start_date, $end_date, $network) 
    {
    	$cachePrefix = 'totalSharesByNetwork-' . $network;
    	$a = $this->db->escape($network);
		$sql = 'SELECT COUNT(id) as `count` FROM shares WHERE network = ' . $a . ' AND created_at BETWEEN ? AND ?';
		return $this->run($sql, $cachePrefix, $start_date, $end_date);
    }


    protected function run($sql, $cachePrefix, $start_date, $end_date)
    {
    	$data = 0;
		$start_date = date('Y-m-d', $start_date) . ' 00:00:00';
		$end_date = date('Y-m-d ', $end_date) . ' 23:59:59';
		$cache_file = $cachePrefix . '-' . $start_date . '-' . $end_date;

		if (cachingOn() && $this->cache->file->get($cache_file)) 
		{ 
			$data = $this->cache->file->get($cache_file);
		} 
		else 
		{
			$query = $this->db->query($sql, array($start_date, $end_date));
			
			if ($query->num_rows() > 0) 
			{ 
				$row = $query->row_array();
				$data = $row['count'];
			}
				
			if (cachingOn())
			{
				$this->cache->file->save($cache_file, $data, cachingTime());
			}
		}
		
		return $data;
    }
}

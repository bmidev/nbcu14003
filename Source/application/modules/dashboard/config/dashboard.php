<?php

/*
|--------------------------------------------------------------------------
| Standard Reporting Dashboard Configuration
|--------------------------------------------------------------------------
|
|	Any function that is used as a shortcut to call any of the config
|	variables on this page can be located in the config helper in the 
|	config directory. (The config helper is auto-loaded)
|	
|	See: ./dashboard/helpers/config_helper.php
|
|	Version: 2.11
|	Last Updated: February 10, 2012
|
|--------------------------------------------------------------------------
| Google Analytics Information
|--------------------------------------------------------------------------
|
|	Call each of these variables using gaEmail(), gaPassword(), or
|	gaProfileId() respectively.
|
|--------------------------------------------------------------------------
| Where to find the Google Analytics profile ID
|--------------------------------------------------------------------------
|
|   To get the ga_profile_id config item, log into http://google.com/analytics
|   with the Brandmover's e-mail and password (below), and then click
|   into the site for your project. The URL will look something like:
|
|   https://www.google.com/analytics/web/?hl=en#report/visitors-overview/a30383075w56852320p57967808/
|
|   The ga_profile_id is the last set of numbers after the 'p' in the url
|   (in this example, 57967808).
|
*/

$config['ga_email'] = 'ga-api@brandmovers.com'; 
$config['ga_password'] = 'Q5unYVd5r17h';
$config['ga_profile_id'] = '87175870';

/*
|--------------------------------------------------------------------------
| HTTPS 
|--------------------------------------------------------------------------
|
|	If an SSL Certificate exists for this site, set this variable to 
|	TRUE to force users to visit via HTTPS. 
|	Call using isHttps();
|
*/

$config['https'] = FALSE;

/*
|--------------------------------------------------------------------------
| Data File Caching
|--------------------------------------------------------------------------
|
|	Use this variable to turn caching on/off. Call this variable 
|	using the cachingOn() function. It will return TRUE if caching  
|	is on and FALSE if caching is turned off.
|
|	Caching time is the number of seconds that data should be cached.
|	Call using cachingTime();
|
*/

$config['caching'] = TRUE;
$config['caching_time'] = 7200;

/*
|--------------------------------------------------------------------------
| Login Page Logo
|--------------------------------------------------------------------------
|
|	The logo image that appears on the login screen. Should be a gif or
|	a png with a transparent background. Background hex color is #EEEEEE.
|	Dimensions: Must fit with-in 435x220 pixels.
|	
|	Call this variable using the function: appLogo()
|
*/

$config['app_logo'] = 'img/logo.gif'; 

/* Padding around your logo image. */
$config['app_logo_padding'] = '50px 0 0 0';  

/*
|--------------------------------------------------------------------------
| Top Bar Icon
|--------------------------------------------------------------------------
|
|	This is the icon that appears in the top bar of every page.
|	Dimensions: 18x18 pixels.
|	
|	Call this variable using the function: appIcon()
|
*/

$config['app_icon'] = 'img/icon.gif'; // Call using appIcon()

/*
|--------------------------------------------------------------------------
| Application Information
|--------------------------------------------------------------------------
|
|	- Application Name appears in the top bar and title of every page.
|	  Function: appName()
|
|   - Application Title - Call item a "Reporting Dashboard" or
|     anything you want. You can even just leave the title area blank.
|
|   - Application Start Date - The day that this promotion began.
|	  Function: appStartDate()
|
|   - Application End Date - The day that this promotion ends.
|	  Function: appEndDate()
|
*/

$config['app_name'] = 'Brandmovers'; 
$config['app_title'] = 'Reporting Dashboard';
$config['app_start_date'] = strtotime('2014-06-16') < time() ? strtotime('2014-06-16') : time(); 	
$config['app_end_date'] = strtotime('2014-06-30'); 		 


/*
|--------------------------------------------------------------------------
| Password Salt
|--------------------------------------------------------------------------
|
|	This is a random string used to pad passwords before going into 
|	sha1. Passwords are encypted and validated using the password helper.
|
*/

$config['password_salt'] = '8qYNUY9JAh6B1wX2Vwl90fhDgCqff2qxMtyscUEmvHlzUy6leeUmk2NJdDag';


/*
|--------------------------------------------------------------------------
| Get the Current Date/Time in Y-m-d H:i:s Format.
|--------------------------------------------------------------------------
|
|	Call this variable using the function: now()
|
*/

$config['current_date'] = date('Y-m-d H:i:s', time());



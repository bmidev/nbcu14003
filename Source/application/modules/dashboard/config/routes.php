<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['dashboard/logout'] = 'login/logout';
$route['dashboard/start_date/(:any)'] = 'dashboard';

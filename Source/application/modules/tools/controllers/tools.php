<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed.');

/**
 * This class provides tools for managing the application.
 *
 * Please note that these tools can only be run from the command line.
 *
 * @package default
 * @author Eric Stewart
 **/
class Tools extends MX_Controller {
	
	/**
	 * This method allows you to see what environment is currently being used.
	 *
	 * COMMAND:
	 * $ php index.php tools environment
	 * @return void
	 * @author Eric Stewart
	 **/
	public function environment() {
		print ENVIRONMENT.PHP_EOL;
	}

	/**
	 * This method allows you to migrate the database.
	 *
	 * COMMAND:
	 * $ php index.php tools migrate [options]
	 * 
	 * HELP:
	 * $ php htdocs/index.php tools migrate -h
	 * 
	 * SAMPLE COMMANDS:
	 * $ php htdocs/index.php tools migrate
	 * $ php htdocs/index.php tools migrate --latest
	 *
	 * @return void
	 * @author Eric Stewart
	 **/
	public function migrate()
	{
		// Ensure the application is run using CLI.
		if (!$this->input->is_cli_request()) die('Web access to this tool is not allowed.');
		
		// Preconfigure options.
		$options = array();
		
		// Fetch the user input.
		$input_argc = $this->input->server('argc');
		$input_argv = $this->input->server('argv');
		
		// Remove script name, controller and method to isolate the arguments.
		$method_position = 0;
		foreach ($input_argv as $key => $value)
		{
			if (strtolower($value) == 'migration')
			{
				$method_position = $key;
			}
		}
		$options = array_splice($input_argv, ($method_position + 1));
		
		// Create the help details.
		$help  = "COMMAND:".PHP_EOL;
		$help .= " $ php htdocs/index.php tools migrate [options]".PHP_EOL;
		$help .= PHP_EOL;
		$help .= "PARAMETERS:".PHP_EOL;
		$help .= " None.".PHP_EOL;
		$help .= PHP_EOL;
		$help .= "OPTIONS:".PHP_EOL;
		$help .= " --check : Will not perform a migration, but instead will return.".PHP_EOL;
		$help .= "   the current migrated version of the application.".PHP_EOL;
		$help .= PHP_EOL;
		$help .= ' --latest : Will ignore the $config[\'migration_version\']'.PHP_EOL;
		$help .= "   setting in CodeIgniter and will use the very newest migration".PHP_EOL;
		$help .= "   found in the filesystem.".PHP_EOL;
		$help .= PHP_EOL;
		$help .= " --version=# : Will migrate the database to version specified.".PHP_EOL;
		$help .= PHP_EOL;
		$help .= "EXAMPLES:".PHP_EOL;
		$help .= " This is an example of migrating to the version specified in the".PHP_EOL;
		$help .= " migration config.".PHP_EOL;
		$help .= PHP_EOL;
		$help .= ' $ php htdocs/index.php tools migrate'.PHP_EOL;
		$help .= PHP_EOL;
		$help .= " This is an example of migrating to the lastest migration.".PHP_EOL;
		$help .= PHP_EOL;
		$help .= ' $ php htdocs/index.php tools migrate --latest'.PHP_EOL;
		$help .= PHP_EOL;
		$help .= " This is an example of migrating to version 23.".PHP_EOL;
		$help .= PHP_EOL;
		$help .= ' $ php htdocs/index.php tools migrate --version=23'.PHP_EOL;
		$help .= PHP_EOL;
		
		// Check for help option.
		if (in_array('-h', $options))
		{
			print $help;
			exit(0);
		}
		
		// Load the configuration.
		$this->config->load('migration');
		
		// Check if migrations are enabled.
		if (!$this->config->item('migration_enabled')) {
			print "The conf setting 'migration_enabled' is set to FALSE. Please set it to TRUE before attempting to run the migration tool.".PHP_EOL;
			exit(1);
		}
		
		// Load the migration library.
		$this->load->library('migration');
		
		// Run a migration version check if specified.
		if (in_array('--check', $options)) {
			$this->load->model('migrate_model', '', TRUE);
			print $this->migrate_model->current_migration().PHP_EOL;
			exit(0);
		}
		
		// Migrate to the latest.
		if (in_array('--latest', $options)) {
			if ($this->migration->latest()) {
				print "Completed migrating to the latest.".PHP_EOL;
				exit(0);
			} else {
				print $this->migration->error_string().PHP_EOL;
				exit(1);
			}
		}
		
		// Migrate to a specific version.
		$version = NULL;
		foreach ($options as $option) {
			if (substr($option, 0, 9) == '--version') {
				$version = (int)substr($option, 10);
			}
		}
		if ($version !== NULL) {
			if ($this->migration->version($version)) {
				print "Completed migrating to version ".$version.".".PHP_EOL;
				exit(0);
			} else {
				print $this->migration->error_string().PHP_EOL;
				exit(2);
			}
		}
		
		// If no other actions were specified, migrate using current.
		if ($this->migration->current()) {
			print "Completed migrating to current version ".$this->config->item('migration_version').".".PHP_EOL;
			exit(0);
		} else {
			print $this->migration->error_string().PHP_EOL;
			exit(3);
		}
	}
	
	/**
	 * This method allows you to synchronize all source files and folders to a destination.
	 *
	 * COMMAND:
	 * $ php index.php tools sync_files [options] destination
	 * 
	 * HELP:
	 * $ php htdocs/index.php tools sync_files -h
	 * 
	 * SAMPLE COMMANDS:
	 * $ php htdocs/index.php tools sync_files ssh:test%test.brandmovers.net:~/
	 * $ php htdocs/index.php tools sync_files ~/
	 *
	 * @return void
	 * @author Eric Stewart
	 **/
	public function sync_files()
	{
		// Ensure the application is run using CLI.
		if (!$this->input->is_cli_request()) die('Web access to this tool is not allowed.');
		
		// Preconfigure options.
		$destination = NULL;
		$destination_type = 'local';
		$options = array();
		
		// Fetch the user input.
		$input_argc = $this->input->server('argc');
		$input_argv = $this->input->server('argv');
		
		// Remove script name, controller and method to isolate the arguments.
		$method_position = 0;
		foreach ($input_argv as $key => $value) {
			if (strtolower($value) == 'sync_files') {
				$method_position = $key;
			}
		}
		$arguments = array_splice($input_argv, ($method_position + 1));
		
		// Create the help details.
		$help  = "COMMAND:".PHP_EOL;
		$help .= " $ php htdocs/index.php tools sync_files [options] destination".PHP_EOL;
		$help .= PHP_EOL;
		$help .= "PARAMETERS:".PHP_EOL;
		$help .= " destination: This is the destination location you are synchronizing files".PHP_EOL;
		$help .= " too. It can be a local folder or a folder on another computer. Currently,".PHP_EOL;
		$help .= " only SSH is available for remote syncing.".PHP_EOL;
		$help .= PHP_EOL;
		$help .= " Local folders should be specified using a full path to the folder such as".PHP_EOL;
		$help .= " '/users/sites/example.com' or '~/Sites'.".PHP_EOL;
		$help .= PHP_EOL;
		$help .= " Remote folders should use the following syntax 'ssh:user@host:/folder'".PHP_EOL;
		$help .= PHP_EOL;
		$help .= "OPTIONS:".PHP_EOL;
		$help .= " --dry-run : Will simulate the file synchronization, but list the files it".PHP_EOL;
		$help .= "   would sync instead of transferring.".PHP_EOL;
		$help .= PHP_EOL;
		$help .= " --delete : Will delete extraneous files from the destination upon file".PHP_EOL;
		$help .= "   synchronization.".PHP_EOL;
		$help .= PHP_EOL;
		$help .= "EXAMPLES:".PHP_EOL;
		$help .= " This is an example of synchronizing to a local folder.".PHP_EOL;
		$help .= PHP_EOL;
		$help .= ' $ php htdocs/index.php tools sync_files ~/'.PHP_EOL;
		$help .= PHP_EOL;
		$help .= " This is an example of synchronizing to a remote folder. It will".PHP_EOL;
		$help .= " synchronize the files to the example.com server using 'mike' as".PHP_EOL;
		$help .= " the user and the home folder of that user as the destination folder.".PHP_EOL;
		$help .= PHP_EOL;
		$help .= ' $ php htdocs/index.php tools sync_files ssh:mike@example.com:~/'.PHP_EOL;
		$help .= PHP_EOL;
		
		
		// Check for help option.
		if (in_array('-h', $arguments)) {
			print $help;
			exit(0);
		}
		
		// Extract the parameters and any options.
		$parameters = array();
		// Loop through all the arguments, if it starts with a '-' it's and option,
		// otherwise it's the parameter.
		foreach ($arguments as $argument) {
			if (substr($argument, 0, 1) == '-') {
				$options[] = strtolower($argument);
			} else {
				$parameters[] = $argument;
			}
		}
		
		// Check that 1 parameter, the destination, was passed.
		if (sizeof($parameters) != 1) {
			print "Invalid command.".PHP_EOL;
			print PHP_EOL;
			print $help;
			exit(1);
		} else {
			$destination = $parameters[0];
		}
		
		// Prepare the destination.
		if (substr(strtolower($destination), 0, 4) == 'ssh:') {
			$destination_type = 'ssh';
			$destination = substr($destination, 4);
		}
		
		// Assemble the synchronization command.
		$source = dirname(BASEPATH).'/';
		
		// Specify rsync with archive, verbose, and compress options.
		$rsync = "rsync -avz";
		
		// If ssh destination set option.
		if ($destination_type == 'ssh') {
			$rsync .= ' --rsh=ssh';
		}
		
		// If dry-run set option
		if (in_array('--dry-run', $options)) {
			$rsync .= ' --dry-run';
		}
		
		// If delete set option.
		if (in_array('--delete', $options)) {
			$rsync .= ' --delete';
		}
		
		// Set excludes.
		$rsync .= " --exclude '.git'";
		$rsync .= " --exclude '.gitignore'";
		$rsync .= " --exclude '.DS_Store'";
		$rsync .= " --exclude '/application/cache/*'";
		$rsync .= " --exclude '/application/data/*'";
		$rsync .= " --exclude '/htdocs/data/*'";
		$rsync .= " --exclude 'logs'";
		$rsync .= " --exclude '.*'";
		
		// Set the source and destination.
		$rsync .= ' '.escapeshellarg($source).' '.escapeshellarg($destination);
		
		// Synchronize the files.
		print 'Synchronizing files.'.PHP_EOL;
		$result = system($rsync);
		if ($result === FALSE) {
			print "File synchronization failed. Halting deployment.".PHP_EOL;
			exit(1);
		}
		
		// Final status and exit.
		print 'Deployment complete!'.PHP_EOL;
		exit(0);
	}
}

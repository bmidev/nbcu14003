Tools Module
===============

Author: Eric Stewart <estewart@brandmovers.com>
Module: `ci/tools`

This is a command-line utility for migrating databases and deploying sites.

Dependencies
------------

* CodeIgniter with [Modular Extensions](https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc/wiki/Home)

Installation
------------

Add this to your project `composer.json`, and then run `composer install` or `composer update`:

```
{
    "require": {
        "ci/tools": "*"
    }
}
```

Usage
-----

### Database Migrations

```
COMMAND:
 $ php htdocs/index.php tools migrate [options]

PARAMETERS:
 None.

OPTIONS:
 --check : Will not perform a migration, but instead will return.
   the current migrated version of the application.

 --latest : Will ignore the $config['migration_version']
   setting in CodeIgniter and will use the very newest migration
   found in the filesystem.

 --version=# : Will migrate the database to version specified.

EXAMPLES:
 This is an example of migrating to the version specified in the
 migration config.

 $ php htdocs/index.php tools migrate

 This is an example of migrating to the lastest migration.

 $ php htdocs/index.php tools migrate --latest

 This is an example of migrating to version 23.

 $ php htdocs/index.php tools migrate --version=23
```

### Site Deployments

```
COMMAND:
 $ php htdocs/index.php tools sync_files [options] destination

PARAMETERS:
 destination: This is the destination location you are synchronizing files
 to. It can be a local folder or a folder on another computer. Currently,
 only SSH is available for remote syncing.

 Local folders should be specified using a full path to the folder such as
 '/users/sites/example.com' or '~/Sites'.

 Remote folders should use the following syntax 'ssh:user@host:/folder'

OPTIONS:
 --dry-run : Will simulate the file synchronization, but list the files it
   would sync instead of transferring.

 --delete : Will delete extraneous files from the destination upon file
   synchronization.

EXAMPLES:
 This is an example of synchronizing to a local folder.

 $ php htdocs/index.php tools sync_files ~/

 This is an example of synchronizing to a remote folder. It will
 synchronize the files to the example.com server using 'mike' as
 the user and the home folder of that user as the destination folder.

 $ php htdocs/index.php tools sync_files ssh:mike@example.com:~/
```

<?php

ini_set('date.timezone', 'America/New_York'); // Set the default timezone to EST.

/*
 *---------------------------------------------------------------
 * P3P Header
 *---------------------------------------------------------------
 *
 * Fixes issues with setting cookies inside iframes in IE browsers
 *
 */
if (php_sapi_name() !== 'cli')
{
	header('P3P: CP="CAO CURa IDC DSP COR ADM ADMa DEVa DEVi TAIi PSA PSD IVAi IVDi CONi COM HIS OUR IND CNT ONL OUR DEM PRE"');
}

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     testing
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */

// Load and run the environment manager if present
$basedir = dirname(dirname(__FILE__));
if (file_exists($basedir . '/application/third_party/phpdotenv/Dotenv.php'))
{
	require_once $basedir . '/application/third_party/phpdotenv/Dotenv.php';
	Dotenv::load($basedir);
}

// Configure the application environment
$envs = array('production', 'development', 'testing');
if (getenv('APPENV') && in_array(strtolower(getenv('APPENV')), $envs))
{
    define('ENVIRONMENT', strtolower(getenv('APPENV')));
}
else
{
    define('ENVIRONMENT', 'production');
}

/*
 *---------------------------------------------------------------
 * Prelaunch filtering
 *---------------------------------------------------------------
 *
 * Blocks access to pre-launch sites by unauthorized personell without requiring passwords or fiddling with .htaccess or .htpasswd
 *
 */

// Filter all but testers and Facebook robots.
if (php_sapi_name() !== 'cli' && ENVIRONMENT !== 'development' && time() < strtotime('2014-06-02 12:00:00'))
{
	$token = 'pBBv2aJCyBPgHzoxYMrltrhhQssQrH1XRuSiSSL4PiBXb6pwvhz7QQlBSh3WEFp';
	$allow = false;

	// Check for prelaunch authorization cookie.
	if (isset($_COOKIE['prelaunch']) && $_COOKIE['prelaunch'] === $token)
	{
		$allow = true;
	}
	
	// Check for Facebook Scraper User Agent.
	if (isset($_SERVER['HTTP_USER_AGENT']) && stripos($_SERVER['HTTP_USER_AGENT'], 'facebookexternalhit') !== false)
	{
		$allow = true;
	}
	
	// Prompt and stop if access is denied.
	if ( ! $allow)
	{
		print "Access Denied.";
		exit();
	}
}

/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but testing and live will hide them.
 */

if (defined('ENVIRONMENT'))
{
	if (version_compare(PHP_VERSION, '5.4.0') >= 0)
		$error_level = E_ALL ^ E_STRICT;
	else
		$error_level = E_ALL;

	switch (ENVIRONMENT)
	{
		case 'development':
			ini_set('error_reporting', $error_level);
			ini_set('display_errors', 'On');
		break;
	
		case 'testing':
		case 'production':
			ini_set('error_reporting', $error_level);
			ini_set('display_errors', '');
		break;

		default:
			exit('The application environment is not set correctly.');
	}
}

/*
 *---------------------------------------------------------------
 * SYSTEM FOLDER NAME
 *---------------------------------------------------------------
 *
 * This variable must contain the name of your "system" folder.
 * Include the path if the folder is not in the same  directory
 * as this file.
 *
 */
	$system_path = dirname(dirname(__FILE__)) . '/system';

/*
 *---------------------------------------------------------------
 * APPLICATION FOLDER NAME
 *---------------------------------------------------------------
 *
 * If you want this front controller to use a different "application"
 * folder then the default one you can set its name here. The folder
 * can also be renamed or relocated anywhere on your server.  If
 * you do, use a full server path. For more info please see the user guide:
 * http://codeigniter.com/user_guide/general/managing_apps.html
 *
 * NO TRAILING SLASH!
 *
 */
	$application_folder = dirname($system_path) . '/application';

/*
 * --------------------------------------------------------------------
 * DEFAULT CONTROLLER
 * --------------------------------------------------------------------
 *
 * Normally you will set your default controller in the routes.php file.
 * You can, however, force a custom routing by hard-coding a
 * specific controller class/function here.  For most applications, you
 * WILL NOT set your routing here, but it's an option for those
 * special instances where you might want to override the standard
 * routing in a specific front controller that shares a common CI installation.
 *
 * IMPORTANT:  If you set the routing here, NO OTHER controller will be
 * callable. In essence, this preference limits your application to ONE
 * specific controller.  Leave the function name blank if you need
 * to call functions dynamically via the URI.
 *
 * Un-comment the $routing array below to use this feature
 *
 */
	// The directory name, relative to the "controllers" folder.  Leave blank
	// if your controller is not in a sub-folder within the "controllers" folder
	// $routing['directory'] = '';

	// The controller class file name.  Example:  Mycontroller
	// $routing['controller'] = '';

	// The controller function you wish to be called.
	// $routing['function']	= '';


/*
 * -------------------------------------------------------------------
 *  CUSTOM CONFIG VALUES
 * -------------------------------------------------------------------
 *
 * The $assign_to_config array below will be passed dynamically to the
 * config class when initialized. This allows you to set custom config
 * items or override any default config values found in the config.php file.
 * This can be handy as it permits you to share one application between
 * multiple front controller files, with each file containing different
 * config values.
 *
 * Un-comment the $assign_to_config array below to use this feature
 *
 */
	// $assign_to_config['name_of_config_item'] = 'value of config item';



// --------------------------------------------------------------------
// END OF USER CONFIGURABLE SETTINGS.  DO NOT EDIT BELOW THIS LINE
// --------------------------------------------------------------------

/*
 * ---------------------------------------------------------------
 *  Resolve the system path for increased reliability
 * ---------------------------------------------------------------
 */

	// Set the current directory correctly for CLI requests
	if (defined('STDIN'))
	{
		chdir(dirname(__FILE__));
	}

	if (realpath($system_path) !== FALSE)
	{
		$system_path = realpath($system_path).'/';
	}

	// ensure there's a trailing slash
	$system_path = rtrim($system_path, '/').'/';

	// Is the system path correct?
	if ( ! is_dir($system_path))
	{
		exit("Your system folder path does not appear to be set correctly. Please open the following file and correct this: ".pathinfo(__FILE__, PATHINFO_BASENAME));
	}

/*
 * -------------------------------------------------------------------
 *  Now that we know the path, set the main path constants
 * -------------------------------------------------------------------
 */
	// The name of THIS file
	define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

	// The PHP file extension
	// this global constant is deprecated.
	define('EXT', '.php');

	// Path to the system folder
	define('BASEPATH', str_replace("\\", "/", $system_path));

	// Path to the front controller (this file)
	define('FCPATH', str_replace(SELF, '', __FILE__));

	// Name of the "system folder"
	define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));


	// The path to the "application" folder
	if (is_dir($application_folder))
	{
		define('APPPATH', $application_folder.'/');
	}
	else
	{
		if ( ! is_dir(BASEPATH.$application_folder.'/'))
		{
			exit("Your application folder path does not appear to be set correctly. Please open the following file and correct this: ".SELF);
		}

		define('APPPATH', BASEPATH.$application_folder.'/');
	}

/*
 * --------------------------------------------------------------------
 * LOAD THE BOOTSTRAP FILE
 * --------------------------------------------------------------------
 *
 * And away we go...
 *
 */
require_once BASEPATH.'core/CodeIgniter.php';

/* End of file index.php */
/* Location: ./index.php */
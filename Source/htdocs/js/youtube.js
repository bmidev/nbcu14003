var tag = document.createElement('script');
tag.src = "http://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// Fires whenever a player has finished loading
function onPlayerReady(event) {

}

// Fires when the player's state changes.
function onPlayerStateChange(event) {
	if(event.data == 1)
    	console.log('VIDEO START');
    else if(event.data == 0)
		console.log('VIDEO COMPLETE');
}

function onYouTubeIframeAPIReady() {
    var player = new YT.Player('videoplayer', {
        height: '500',
        width: '830',
        videoId: 'XxdDlIdkFj4',
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}
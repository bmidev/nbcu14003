function record_click(msg)
{
	// _gaq.push(['_trackEvent', 'Landing Page', 'Click', msg+'_'+location.href]);
	ga('send', 'event', 'button', 'click', 'nav-buttons', msg);
}

$(document).ready(function() {

    // $("#dob").inputmask("mask", {"mask": "99-99-9999", "placeholder": "MM-DD-YYYY"});
    $("#dob").inputmask("mask", {"mask": "99/99/9999", "placeholder": "MM/DD/YYYY"});

    $(".return-home-btn").click(function(e) {
        $("#thanks-modal").modal('hide');
        e.preventDefault();
    });

	// $('#dynamic-modal').modal('show');

	$(".german-enter").click(function(e) {
		e.preventDefault();
		$('#german-registration-modal').modal('show');
		ga('send', 'event', 'button', 'click', 'nav-buttons', 'gm_fb_image');
	});


	$("#submit-button").click(function() {
		// $("label.textarea-caption").closest('span.error').addClass('captiony');
		// alert('clicked');
     
        // $('.checkbox-label').next('span').addClass('official_rules_error');

		$("span.required-fields").hide();
		$("span.required-fields-error").show();
	});

    $("#german-registration-form").validate({

        rules: {
            email: {
                required: true,
                email: true,
            },
            caption: {
            	required: true,
            	maxlength: 140,
            },
            dob: {
                required: true,
                date: true,
            },
        },

        messages: {
            first_name: "Bitte dieses Feld ausfüllen",
            last_name: "Bitte dieses Feld ausfüllen",
            dob: {
                requred: "Bitte dieses Feld ausfüllen",
                date: "MM/DD/YYYY",
            },

            email: {
                required: "Bitte dieses Feld ausfüllen",
            },

            official_rules: "Bitte dieses Feld ausfüllen",
            caption: {
                required: "Bitte dieses Feld ausfüllen",
                maxlength: "max. 140 Zeichen",
            },
        },

        errorElement: "span",

        errorPlacement: function(error, element) {
        	error.insertAfter(element.parent().children("label"));
    	},

    	highlight: function(element) {
			jQuery(element).siblings("label").addClass("error");

            // $('.checkbox-label').next('span').addClass('official_rules_error');
            
			returnVar = 'fal';
		},

	    submitHandler: function() {

	        $.ajax({
	            url: $("#german-registration-form").attr("action"),
	            data: $("#german-registration-form").serialize(),
	            type: 'POST',
                dataType: 'json',

                error: function (jxhr, msg, err) 
                {
                    dynamic_modal("ERROR", "THERE WAS AN ERROR", msg);
                },

                success: function(data, textStatus, jqXHR) 
                {
                    if(data.success == false)
                    {
                    	dynamic_modal("ERROR", "THERE WAS AN ERROR", data.error);
                    } else {
                    	$("#german-registration-modal").modal('hide');
                        $('#thanks-modal').modal('show');
                        $('input[type="submit"]').attr('disabled','disabled');
                        // dynamic_modal("THANKS", "YOUR ENTRY WAS SAVED");
                    }
                }
	        });
	    }
    });

	function dynamic_modal(main_title,  body_title, body_msg)
    {
        $('#dynamic-modal').modal('show');
        $('.modal-title').show().html(body_msg);
        $('#dynamic-body-title').show().html(body_title);
        $('#dynamic-body-message').show().html(body_msg);
    }
});